if (process.env.DEPLOYMENT === 'ci') {
  // eslint-disable-next-line @typescript-eslint/no-var-requires
  process.env.CHROME_BIN = require('puppeteer').executablePath();
  console.log('set CHROME_BIN to', process.env.CHROME_BIN);
}

// Karma configuration
// Generated on Tue Mar 28 2023 14:33:48 GMT+0200 (Central European Summer Time)

module.exports = function (config) {
  config.set({
    // pass ENV params to karma tests using window.__karma__.config.env
    client: {
      env: {
        MINTBLUE_SDK_TOKEN: process.env.MINTBLUE_SDK_TOKEN,
        MINTBLUE_API_URL: process.env.MINTBLUE_API_URL,
      },
    },

    // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: '.',

    // frameworks to use
    // available frameworks: https://www.npmjs.com/search?q=keywords:karma-adapter
    frameworks: ['mocha', 'chai'],

    // list of files / patterns to load in the browser
    files: [
      'dist/browser/index.umd.js',
      'https://cdn.jsdelivr.net/gh/ethereumjs/browser-builds/dist/ethereumjs-tx/ethereumjs-tx-1.3.3.min.js',
      'node_modules/lodash/lodash.min.js',
      'test/mocks.js',
      'test/browser/init.js',
      'test/*.test.js',
    ],

    // list of files / patterns to exclude
    exclude: [],

    // I had to list all plugins
    plugins: [
      'karma-sourcemap-loader',
      'karma-mocha',
      'karma-chai',
      'karma-chrome-launcher',
      ...(process.env.DEPLOYMENT === 'ci' ? [] : ['karma-firefox-launcher']),
      'karma-spec-reporter',
    ],

    // preprocess matching files before serving them to the browser
    // available preprocessors: https://www.npmjs.com/search?q=keywords:karma-preprocessor
    preprocessors: {
      '**/*.js': ['sourcemap'],
    },

    // test results reporter to use
    // possible values: 'dots', 'progress'
    // available reporters: https://www.npmjs.com/search?q=keywords:karma-reporter
    reporters: ['spec'],

    // web server port
    port: 9876,

    // enable / disable colors in the output (reporters and logs)
    colors: true,

    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,

    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: false,

    // start these browsers
    // available browser launchers: https://www.npmjs.com/search?q=keywords:karma-launcher
    browsers:
      process.env.DEPLOYMENT === 'ci'
        ? ['ChromeCI'] // Use limited set in CI
        : [
            'ChromeHeadless',
            // 'ChromeDebugging',
            'FirefoxHeadless',
          ],

    // necessary for debugging tests (debugger attaches to Chrome (not Node))
    customLaunchers: {
      ChromeDebugging: {
        base: 'Chrome',
        flags: ['--remote-debugging-port=9333'],
      },
      ChromeCI: {
        base: 'ChromeHeadless',
        flags: ['--no-sandbox'], // required by Cypress image in CI
      },
    },

    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: false,

    // Concurrency level
    // how many browser instances should be started simultaneously
    concurrency: Infinity,
  });
};
