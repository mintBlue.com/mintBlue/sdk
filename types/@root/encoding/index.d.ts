declare module '@root/encoding/base64.js' {
  interface EncModule {
    bufToBase64(buf: Buffer | Uint8Array): string;
    strToBase64(str: string): string;
    base64ToBuf(b64: string): Buffer;
    base64ToStr(b64: string): string;
    urlBase64ToBase64(u64: string): string;
    base64ToUrlBase64(b64: string): string;
    bufToUrlBase64(buf: Buffer | Uint8Array): string;
    strToUrlBase64(str: string): string;
  }

  const Enc: EncModule;
  export = Enc;
}

declare module '@root/encoding/hex.js' {
  interface EncModule {
    bufToHex(buf: Buffer | Uint8Array): string;
    strToHex(str: string): string;
    hexToBuf(hex: string): Buffer;
    hexToStr(hex: string): string;
    numToHex(d: number): string;
  }

  const Enc: EncModule;
  export = Enc;
}

declare module '@root/encoding/bytes.js' {
  interface EncModule {
    binToBuf(bin: string): Buffer;
    binToStr(bin: string): string;
    bufToBin(buf: Buffer | Uint8Array): string;
    bufToStr(buf: Buffer | Uint8Array): string;
    strToBin(str: string): string;
    strToBuf(str: string): Buffer;
    base64ToHex(b64: string): string;
    hexToBase64(hex: string): string;
  }

  const Enc: EncModule;
  export = Enc;
}
