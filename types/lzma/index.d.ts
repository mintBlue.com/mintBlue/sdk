interface _lzma {
  compress(
    str: string | Buffer,
    mode: number,
    // The "result" is normal array of numbers - signed byte (from -127 to 128). You can convert with "new Int8Array(result)"
    onFinish: (result: number[] | null, error: Error) => void,
    onProgress?: (progress: number) => void,
  ): void;

  decompress(
    byteArray: ArrayLike<number>,
    onFinish: (result: Buffer, error: Error) => void,
    onProgress?: (progress: number) => void,
  ): void;

  disableEndMark: boolean;
}

declare module 'lzma' {
  const LZMA: _lzma;
  export default LZMA;
}

declare module 'lzma/src/lzma_worker.js' {
  export const LZMA: _lzma;
  export const LZMA_WORKER: _lzma;
}
