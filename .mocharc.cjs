module.exports = {
  extensions: ['js'],
  'node-option': ['experimental-specifier-resolution=node', 'loader=ts-node/esm'],
};
