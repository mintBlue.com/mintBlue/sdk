import path from 'path';
import esbuild from 'esbuild';
import { polyfillNode } from 'esbuild-plugin-polyfill-node';
import alias from 'esbuild-plugin-alias';

esbuild
  .build({
    entryPoints: ['./src/index.ts'],
    outfile: path.resolve(process.cwd(), 'dist', 'browser', 'index.umd.js'),
    sourcemap: 'inline',
    platform: 'browser',
    format: 'iife',
    bundle: true,
    globalName: 'mintblue',
    plugins: [
      alias({
        entries: [{ find: 'bsv', replacement: '@ts-bitcoin/core' }],
      }),
      polyfillNode({
        // You might need to adjust polyfills based on your needs
      }),
    ],
    loader: {
      '.ts': 'ts',
    },
    define: {
      'process.env.NODE_ENV': '"production"',
    },
    tsconfig: path.resolve(process.cwd(), './tsconfig/browser.json'),
    external: [],
  })
  .then((v) => {
    console.log(v);
  })
  .catch((e) => {
    console.log(e);
    throw new Error('Build failed.');
  });
