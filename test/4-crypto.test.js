async function compareKeys(key1, key2) {
  chai.assert.equal(key1.publicKeyHex, key2.publicKeyHex);
  chai.assert.equal(key1.privateKey.toWif(), key2.privateKey.toWif());
  chai.assert.equal(
    (await mintblue.utils.exportKey(key1.masterKey)).toString(),
    (await mintblue.utils.exportKey(key2.masterKey)).toString(),
  );
  chai.assert.equal(
    (await mintblue.utils.exportKey(key1.kek)).toString(),
    (await mintblue.utils.exportKey(key2.kek)).toString(),
  );
}

describe('Crypto', () => {
  it('Can derive keys from password consistently', async () => {
    const salt = mintblue.utils.generateSaltToDeriveKey();
    const { salt: salt1, rawKey: rawKey1 } = await mintblue.utils.deriveKey('password', salt);
    const { salt: salt2, rawKey: rawKey2 } = await mintblue.utils.deriveKey('password', salt);

    chai.assert.equal(salt, salt1);
    chai.assert.equal(salt1, salt2);
    chai.assert.equal(Buffer.from(rawKey1).toString('hex'), Buffer.from(rawKey2).toString('hex'));
  });

  it('Can create userKeyAttributes from password and then decrypt the masterKey', async () => {
    const { userKeyAttributes, masterKey } = await mintblue.utils.generateUserKeyAttributes('password');

    const generatedKeys = await mintblue.utils.getKeysFromUserKeyAttributes('password', userKeyAttributes);

    chai.assert.equal(userKeyAttributes.publicKeyHex, generatedKeys.publicKeyHex);
    chai.assert.equal(
      (await mintblue.utils.exportKey(masterKey)).toString(),
      (await mintblue.utils.exportKey(generatedKeys.masterKey)).toString(),
    );
  });

  it('Can recover with recovery key', async () => {
    const { userKeyAttributes, masterKey } = await mintblue.utils.generateUserKeyAttributes('password');

    const generatedKeys = await mintblue.utils.getKeysFromUserKeyAttributes('password', userKeyAttributes);

    const recoveryKey = await mintblue.utils.getRecoveryKey(generatedKeys, userKeyAttributes);

    const recoveredUserKeyAttributes = await mintblue.utils.recoverUserKeyAttributes(
      userKeyAttributes,
      'newpassword',
      mintblue.utils.hex.bufToHex(recoveryKey),
    );

    const restoredKeys = await mintblue.utils.getKeysFromUserKeyAttributes(
      'newpassword',
      recoveredUserKeyAttributes.userKeyAttributes,
    );

    chai.assert.equal(userKeyAttributes.publicKeyHex, generatedKeys.publicKeyHex);
    chai.assert.equal(userKeyAttributes.publicKeyHex, recoveredUserKeyAttributes.userKeyAttributes.publicKeyHex);
    chai.assert.equal(
      (await mintblue.utils.exportKey(masterKey)).toString(),
      (await mintblue.utils.exportKey(generatedKeys.masterKey)).toString(),
    );
    chai.assert.equal(
      (await mintblue.utils.exportKey(restoredKeys.masterKey)).toString(),
      (await mintblue.utils.exportKey(generatedKeys.masterKey)).toString(),
    );
  });

  it('Can serialize and deserialize keys', async () => {
    const { userKeyAttributes } = await mintblue.utils.generateUserKeyAttributes('password');
    const keys = await mintblue.utils.getKeysFromUserKeyAttributes('password', userKeyAttributes);

    const serialized = await mintblue.utils.serializeKeys(keys);

    const deserializedKeys = await mintblue.utils.deserializeKeys(serialized);

    await compareKeys(keys, deserializedKeys);
  });

  it('Can save and retreive keys from Storage', async () => {
    const storageBackend = {};
    const storage = {
      setItem: (key, value) => {
        storageBackend[key] = value;
      },
      getItem: (key) => {
        return storageBackend[key];
      },
    };

    const { userKeyAttributes } = await mintblue.utils.generateUserKeyAttributes('password');
    const keys = await mintblue.utils.getKeysFromUserKeyAttributes('password', userKeyAttributes);

    await mintblue.utils.saveKeysInStorage(keys, storage);

    chai.assert.exists(storageBackend.keys, 'keys was expected to exist in storage');

    const loadedKeys = await mintblue.utils.loadKeysFromStorage(storage);

    await compareKeys(keys, loadedKeys);
  });
});
