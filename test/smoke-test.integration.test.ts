import { assert } from 'chai';
import * as mintblue from '../src/index';
import { createMintblueSDKInstance } from './node/helper';

const mochaTestProjectName = 'project from mocha integration test';

const preExistingProject = {
  id: 'c20d33b8-00a6-4a2f-8bc1-be91e4377e3c',
  txid: '64ab76e345c8b559444c9ad5cf3aa65c6243f7b345bf7f98d6862d3cdb9a8007',
};

async function removeTestProjectIfExist(mb: mintblue.Mintblue) {
  const project = (await mb.listProjects()).find(({ name }) => name === mochaTestProjectName);
  if (project) {
    await mb.destroyProject({
      id: project.id,
    });
  }
}

describe('Smoke tests', () => {
  let mb: mintblue.Mintblue;

  before(async () => {
    mb = await createMintblueSDKInstance();
    assert.exists(mb);

    await removeTestProjectIfExist(mb);
  });

  it('should create a Project', async () => {
    // arrange
    const projectsBefore = await mb.listProjects();
    assert.isAtLeast(projectsBefore.length, 1);

    // act
    await mb.createProject({
      name: mochaTestProjectName,
      description: 'this project was created from our SDK integration tests',
    });

    // assert
    const projectsAfter = await mb.listProjects();
    assert.equal(projectsBefore.length + 1, projectsAfter.length);
    assert.exists(projectsAfter.find((x) => x.name === mochaTestProjectName));
  });

  it('should create a TX with a Stringified JSON obj as data', async () => {
    // arrange
    const data: string = JSON.stringify({
      name: 'mongo-test',
      email: 'john@doe.com',
    });
    const txContent: mintblue.CreateTransactionOptions = {
      project_id: preExistingProject.id,
      outputs: [
        {
          type: 'data',
          value: data,
          encrypt: true,
          sign: true,
        },
      ],
    };

    // act
    const txCreated = await mb.createTransaction(txContent);
    const txn = await mb.getTransaction({
      txid: txCreated.txid,
      parse: true,
    });

    // assert
    assert.equal(txCreated.rawtx, txn.rawtx);

    const dataOutput = txn.outputs[0] as mintblue.DataOutput;
    const contentRaw = dataOutput.value as string;
    const content = JSON.parse(contentRaw.toString());
    assert.equal(content.name, 'mongo-test');
    assert.equal(content.email, 'john@doe.com');
  });

  it('should create a TX with a Buffer as data and show up in listTransactions', async () => {
    // arrange
    const data: Buffer = Buffer.from(
      JSON.stringify({
        name: 'mongo-test',
        email: 'john@doe.com',
      }),
    );
    const txContent: mintblue.CreateTransactionOptions = {
      project_id: preExistingProject.id,
      outputs: [
        {
          type: 'data',
          value: data,
          encrypt: true,
          sign: true,
        },
      ],
    };

    // act
    const txCreated = await mb.createTransaction(txContent);

    // assert
    // is the last created transaction the same one as from listTransactions
    const txs = await mb.listTransactions({
      project_id: preExistingProject.id,
      sort: 'created_at',
      order: 'desc',
      limit: 10,
    });
    assert.equal(txCreated.txid, txs[0].txid);
    assert.isAtMost(txs.length, 10);

    const txn = await mb.getTransaction({
      txid: txCreated.txid,
      parse: true,
    });
    const dataOutput = txn.outputs[0] as mintblue.DataOutput;
    const contentBuffer = Buffer.from(dataOutput.value as Buffer);
    const content = JSON.parse(contentBuffer.toString());
    assert.equal(content.name, 'mongo-test');
    assert.equal(content.email, 'john@doe.com');
  });

  it('should read a TX (getTransaction) with a Buffer as data', async () => {
    // act
    const txn = await mb.getTransaction({
      txid: preExistingProject.txid,
      parse: true,
    });

    // arrange
    const dataOutput = txn.outputs[0] as mintblue.DataOutput;
    const contentBuffer = dataOutput.value as Buffer;
    const content = JSON.parse(contentBuffer.toString());
    assert.equal(content.name, 'integration-test');
    assert.equal(content.email, 'integrations@mintBlue.com');
  });
});
