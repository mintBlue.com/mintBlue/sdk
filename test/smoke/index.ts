import * as mintblue from '../../src/index.js';

if (!process.env.SMOKE_TEST_API_TOKEN) {
  console.error('Please pass SMOKE_TEST_API_TOKEN as inline env variable');
  process.exit(1);
}

// 0. create
// 1. createProject
// 2. listProjects
// 3. getProject
// 4. updateProject
// 5. createTransaction
// 6. listTransactions
// 7. getTransaction
// 8. createAccesstoken
// 9. destroyProject
// 10. createEventListener
// 11. listEventListeners
// 12. getEventListener
// 13. experimentalStreamProject
// 14. experimentalGetPeppolKeyInfo
// 15. experimentalGetKVKKeyInfo
// 16. experimentalGetSigningKeyInfo
// 17. experimentalGetDeriveKeyInfo

let client: mintblue.Mintblue;
let createdProjectId: string = '';
let createdTransactionId: string = '';
let createdEventListenerId: string = '';

try {
  client = await mintblue.Mintblue.create({
    token: process.env.SMOKE_TEST_API_TOKEN,
    url: 'http://localhost:8777',
    version: '4',
  });
  console.log('SUCCESS: initialized client');
  console.log('---------------------------------');
} catch (error) {
  console.log('COULD NOT CREATE CLIENT');
  console.log('error: ', error);
  process.exit(1);
}

try {
  const res = await client.createProject({ name: 'test', description: 'test' });
  createdProjectId = res.id;
  console.log(`SUCCESS: created project ${res.id}`);
  console.log('---------------------------------');
} catch (error: any) {
  console.log('error: ', error.response?.data || error.message);
  console.log('ERROR WITH createProject');
  process.exit(1);
}

try {
  const res = await client.listProjects();
  console.log(`SUCCESS: listed projects, found ${res.length} projects`);
  console.log('---------------------------------');
} catch (error: any) {
  console.log('error: ', error.response?.data || error.message);
  console.log('ERROR WITH listProjects');
  process.exit(1);
}

try {
  const res = await client.getProject({ id: createdProjectId });
  console.log(`SUCCESS: got project ${res.id}`);
  console.log('---------------------------------');
} catch (error: any) {
  console.log('error: ', error.response?.data || error.message);
  console.log('ERROR WITH getProject');
  process.exit(1);
}

try {
  const res = await client.updateProject({ id: createdProjectId, data: { name: 'test1' } });
  console.log(`SUCCESS: updated project ${res.id} name to ${res.name}`);
  console.log('---------------------------------');
} catch (error: any) {
  console.log('error: ', error.response?.data || error.message);
  console.log('ERROR WITH updateProject');
  process.exit(1);
}

try {
  const res = await client.createProject({ name: 'test_destroy', description: 'test_destroy' });
  const projectId = res.id;
  try {
    await client.destroyProject({ id: projectId });
    console.log(`SUCCESS: destroyed project ${res.id}`);
    console.log('---------------------------------');
  } catch (error: any) {
    console.log('error: ', error.response?.data || error.message);
    console.log('ERROR WITH destroyProject');
    process.exit(1);
  }
} catch (error: any) {
  console.log('error: ', error.response?.data || error.message);
  console.log('ERROR WITH createProject');
  process.exit(1);
}

try {
  const outputs = [{ type: 'data', value: 'Hello world', sign: true, encrypt: true }] as any;
  const res = await client.createTransaction({ project_id: createdProjectId, outputs });
  createdTransactionId = res.txid;
  console.log(`SUCCESS: created transaction ${res.txid}`);
  console.log('---------------------------------');
} catch (error: any) {
  console.log('error: ', error.response?.data || error.message);
  console.log('ERROR WITH createTransaction');
  process.exit(1);
}

try {
  const res = await client.getTransaction({ txid: createdTransactionId, parse: true });
  console.log(`SUCCESS: got transaction ${res.txid}`);
  console.log('---------------------------------');
} catch (error: any) {
  console.log('error: ', error.response?.data || error.message);
  console.log('ERROR WITH getTransaction');
  process.exit(1);
}

try {
  const res = await client.listTransactions({ project_id: createdProjectId });
  console.log(`SUCCESS: listed transactions, found ${res.length} transactions`);
  console.log('---------------------------------');
} catch (error: any) {
  console.log('error: ', error.response?.data || error.message);
  console.log('ERROR WITH listTransactions');
  process.exit(1);
}

try {
  const res = await client.createAccesstoken({
    name: 'test',
    // expires_at?: Date;
    // scopes?: Scope[];
  });
  console.log(`SUCCESS: created accesstoken. SDK: ${res.sdkToken}, API: ${res.apiToken}`);
  console.log('---------------------------------');
} catch (error: any) {
  console.log('error: ', error.response?.data || error.message);
  console.log('ERROR WITH createAccesstoken');
  process.exit(1);
}

try {
  const newProject = await client.createProject({ name: 'test', description: 'test' });

  const res = await client.createEventListener({
    name: 'test',
    trigger: { type: 'mintblue.transaction.created', options: { project_id: createdProjectId, txo: '1337' } },
    actions: [{ type: 'insert_into_project' as any, options: { project_id: newProject.id } }],
  });
  createdEventListenerId = res.id;
  console.log(`SUCCESS: created event listener ${res.id}`);
  console.log('---------------------------------');
} catch (error: any) {
  console.log('error: ', error.response?.data || error.message);
  console.log('ERROR WITH createEventListener');
  process.exit(1);
}

try {
  const res = await client.listEventListeners();
  console.log(`SUCCESS: listed event listeners, found ${res.length} listeners`);
  console.log('---------------------------------');
} catch (error: any) {
  console.log('error: ', error.response?.data || error.message);
  console.log('ERROR WITH listEventListeners');
  process.exit(1);
}

try {
  const res = await client.getEventListener({ id: createdEventListenerId });
  console.log(`SUCCESS: got event listener ${res.id}`);
  console.log('---------------------------------');
} catch (error: any) {
  console.log('error: ', error.response?.data || error.message);
  console.log('ERROR WITH getEventListener');
  process.exit(1);
}

try {
  await client.experimentalStreamProject({
    project_id: createdProjectId,
    history: false,
    onTransaction: (tx) => console.log(tx),
    onError: (error) => console.log(error),
  });
  console.log('experimentalStreamProject SUCCESS');
  console.log('---------------------------------');
} catch (error: any) {
  console.log('error: ', error.response?.data || error.message);
  console.log('ERROR WITH experimentalStreamProject');
  process.exit(1);
}

try {
  const res = await client.experimentalGetPeppolKeyInfo();
  console.log(`SUCCESS: experimentalGetPeppolKeyInfo ${res.info}`);
  console.log('---------------------------------');
} catch (error: any) {
  console.log('error: ', error.response?.data || error.message);
  console.log('ERROR WITH experimentalGetPeppolKeyInfo');
  process.exit(1);
}

try {
  const res = await client.experimentalGetKVKKeyInfo();
  console.log(`SUCCESS: experimentalGetKVKKeyInfo ${res.info}`);
  console.log('---------------------------------');
} catch (error: any) {
  console.log('error: ', error.response?.data || error.message);
  console.log('ERROR WITH experimentalGetKVKKeyInfo');
  process.exit(1);
}

try {
  const res = await client.experimentalGetSigningKeyInfo();
  console.log(`SUCCESS: experimentalGetSigningKeyInfo ${res.info}`);
  console.log('---------------------------------');
} catch (error: any) {
  console.log('error: ', error.response?.data || error.message);
  console.log('ERROR WITH experimentalGetSigningKeyInfo');
  process.exit(1);
}

try {
  const res = await client.experimentalGetDeriveKeyInfo();
  console.log(`SUCCESS: experimentalGetDeriveKeyInfo ${res.info}`);
  console.log('---------------------------------');
} catch (error: any) {
  console.log('error: ', error.response?.data || error.message);
  console.log('ERROR WITH experimentalGetDeriveKeyInfo');
  process.exit(1);
}

console.log('ALL SMOKE TESTS PASSED');
process.exit(0);
