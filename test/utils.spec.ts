import { assert } from 'chai';
import el from 'elliptic';

import { utils } from '../src/index.js';
import { getHexKeyInfo, getSHA1Fingerprint, JWKToUncompressedHex } from '../src/lib/utils.js';

describe('Utils', () => {
  it('Can decompress keys', async () => {
    const ec = new el.ec('p256');

    const keys: any = {
      even: {
        compactKey: undefined,
        key: undefined,
      },
      odd: {
        compactKey: undefined,
        key: undefined,
      },
    };

    while (!(keys.even.key !== undefined && keys.odd.key !== undefined)) {
      const kp = ec.genKeyPair();
      const compactKey = kp.getPublic(true, 'hex');
      const key = kp.getPublic(false, 'hex');

      if (compactKey.startsWith('03')) {
        keys.odd.key = key;
        keys.odd.compactKey = compactKey;
      }
      if (compactKey.startsWith('02')) {
        keys.even.key = key;
        keys.even.compactKey = compactKey;
      }
    }

    const evenKeyInfo = utils.getHexKeyInfo(keys.even.compactKey, 'p256');
    assert.deepEqual(evenKeyInfo.publicKey, keys.even.key, 'even uncompressed mismatch');
    assert.deepEqual(evenKeyInfo.compactPublicKey, keys.even.compactKey, 'even compressed mismatch');

    const oddKeyInfo = utils.getHexKeyInfo(keys.odd.compactKey, 'p256');
    assert.deepEqual(oddKeyInfo.publicKey, keys.odd.key, 'odd uncompressed mismatch');
    assert.deepEqual(oddKeyInfo.compactPublicKey, keys.odd.compactKey, 'odd compressed mismatch');
  });

  it('Can compress keys', async () => {
    const ec = new el.ec('p256');
    const kp = ec.genKeyPair();

    const compactkey = kp.getPublic(true, 'hex');
    const key = kp.getPublic(false, 'hex');

    const keyInfo = utils.getHexKeyInfo(key);

    assert.deepEqual(keyInfo.publicKey, key, 'uncompressed mismatch');
    assert.deepEqual(keyInfo.compactPublicKey, compactkey, 'compressed mismatch');
  });

  it('Can generate a SHA-1 fingerprint of an ECC key', async () => {
    const expectedFingerprint = '9c1d1637792f70ec653c33339c04e5802208ea75';
    const uncompressedPubkey =
      '04961d06232b73cf210aafea39927b584b90eff839ce375350714e268a16eeb2e74de6344bbc08ced88c817e28bd80cacddff00fb93f2460b7dbf31ee0167c6202';

    const jwk = {
      kty: 'EC',
      crv: 'P-256',
      x: 'lh0GIytzzyEKr-o5kntYS5Dv-DnON1NQcU4mihbusuc',
      y: 'TeY0S7wIztiMgX4ovYDKzd_wD7k_JGC32_Me4BZ8YgI',
    };

    const keyInfo = getHexKeyInfo(JWKToUncompressedHex(jwk));

    const fingerprint = await getSHA1Fingerprint(jwk);

    assert.deepEqual(expectedFingerprint, fingerprint);
    assert.deepEqual(keyInfo.publicKey, uncompressedPubkey);
  });
});
