import * as mintblue from '../../src/index';

export async function createMintblueSDKInstance(options?: { token?: string; url?: string; mockApi?: any }) {
  const { token, url, mockApi } = options ?? {};
  if (mockApi)
    return new mintblue.Mintblue({
      // @ts-expect-error "mockApi is not specified as valid option, but it is accepted"
      mockApi,
      keys: {} as any,
    });
  return await mintblue.Mintblue.create({
    token: token ?? process.env.MINTBLUE_SDK_TOKEN!,
    url: url ?? process.env.MINTBLUE_API_URL ?? 'https://testapi.mintblue.com',
  });
}
