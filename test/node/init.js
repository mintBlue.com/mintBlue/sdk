/* eslint-disable @typescript-eslint/no-unused-vars */
import * as chai from 'chai';
import _ from 'lodash';

import * as mintblue from '../../src/index.js';

import { createMintblueSDKInstance } from './helper.js';

if (!process.env.MINTBLUE_SDK_TOKEN)
  throw new Error('Env variable MINTBLUE_SDK_TOKEN must be set when running the tests.');

global.chai = chai;
global.mintblue = mintblue;
global._ = _;
global.createMintblueSDKInstance = createMintblueSDKInstance;
