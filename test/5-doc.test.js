const pigiv1Test = {
  filename: 'Hello',
  mimetype: 'mime/test',
  data: new TextEncoder().encode('hello'),
};
const docv1Test = {
  type: 'doc',
  options: {
    filename: pigiv1Test.filename,
    mimetype: pigiv1Test.mimetype,
    meta: 'hello',
  },
  iterations: 200000,
};

const SDKUser2 = {
  token: 'secret-token:mintBlue.com/sdk/IzNRBog7jmwC.lD6IR7idilvs5tRY2G7x',
  userKeyAttributes: {
    masterKeyEncryptedWithKek: 'yoflxnUPwMoDpNGQGUH6RpCb6S7YUrl5uYYIO731gzGPTkpxoqEXpvroHpMoS5RKlYu7P9+/HV9DC083',
    masterKeyEncryptedWithRecoveryKey:
      'kTqezl2j3zjWDLWAy6WSh6SuJNI41l3Gs8OpfbZkapdeC97LP4D3krOZJPT0jP3rx+KonElwjkZp19+k',
    recoveryKeyEncryptedWithMasterKey:
      '2kKnyt34BjRHMs09ZiYmcprSgXtgsmnOwj/cay62Wf4HpnC67SkF14+b/BHgNUxAr4mCS2bmGu3ZeUO2',
    privateKeyEncryptedWithMasterKey:
      'k6ahtkOoc3CMrFoxZ5v3L+j+d6mCnzsTyMvDQHrcMn0QrHvWzsBkO4mtqU8FIPVY9Oor/uRiiZDcS3EHp9o=',
    publicKeyHex: '025d24a20480233fb04010a3ae448cfde6757dce1ce02a8ee9a2e9913173db6a73',
    kekSalt: 'g5mJ80ma4seNsdsXCWmCPvsv34WbnCa0XYN4nBGEHh8=',
    kekIterations: 200000,
  },
  peppolUncompressedPublicKey:
    '04313e3ef5a75be037fb7579948a31b2eb44b3e6477187ba5eef65ede8de0ce9219de9138fb156c3d44698510f59725dd9a1731ae27fc3ef32abac34112dd1fbad',
};

describe('Doc payload', () => {
  it('Doc v1 created and parsed', async () => {
    const signingKeyPair = await mintblue.utils.crypto.subtle.generateKey(
      {
        name: 'ECDSA',
        namedCurve: 'P-256',
      },
      true,
      ['sign', 'verify'],
    );

    const encryptionKeyPair = await mintblue.utils.crypto.subtle.generateKey(
      {
        name: 'ECDH',
        namedCurve: 'P-256',
      },
      true,
      ['deriveKey'],
    );

    //const kp = await mintblue.utils.jose.generateKeyPair('ES256');

    const jwkSigningKey = await mintblue.utils.jose.exportJWK(signingKeyPair.privateKey);
    const jwkEncryptionPublicKey = await mintblue.utils.jose.exportJWK(encryptionKeyPair.publicKey);
    const jwkDecryptionPrivateKey = await mintblue.utils.jose.exportJWK(encryptionKeyPair.privateKey);

    const p = await mintblue.envelopes.DocV1.DocV1.create({
      data: pigiv1Test.data,
      options: {
        filename: pigiv1Test.filename,
        mimetype: pigiv1Test.mimetype,
        meta: 'hello',
      },
      signers: [jwkSigningKey],
      type: 'doc',
      iterations: 200000,
      receivers: [jwkEncryptionPublicKey],
    });

    chai.assert.equal(p.info.encryptedData.length > 3, true);

    const parsedWithSecrets = await mintblue.envelopes.DocV1.DocV1.parse({
      encryptedData: p.info.encryptedData,
      secret1: p.info.secret1,
      secret2: p.info.secret2,
      receiverKey: null,
      verifyKey: signingKeyPair.publicKey,
    });

    chai.assert.deepEqual(parsedWithSecrets.info.data, pigiv1Test.data);
    chai.assert.deepEqual(parsedWithSecrets.info.options.filename, pigiv1Test.filename);
    chai.assert.deepEqual(parsedWithSecrets.info.options.mimetype, pigiv1Test.mimetype);

    const parsedWithReceiverKey = await mintblue.envelopes.DocV1.DocV1.parse({
      encryptedData: p.info.encryptedData,
      secret1: null,
      secret2: null,
      receiverKey: jwkDecryptionPrivateKey,
      verifyKey: signingKeyPair.publicKey,
    });
    chai.assert.deepEqual(parsedWithReceiverKey.info.data, pigiv1Test.data);
    chai.assert.deepEqual(parsedWithReceiverKey.info.options.filename, pigiv1Test.filename);
    chai.assert.deepEqual(parsedWithReceiverKey.info.options.mimetype, pigiv1Test.mimetype);
  });

  it('Mintblue should be able to add a user key as part of the receivers and decrypt a doc v1', async () => {
    const mb = await mintblue.Mintblue.create({
      token: SDKUser2.token,
      userKeyAttributes: SDKUser2.userKeyAttributes,
      url: 'https://testapi.mintblue.com',
    });

    const signing = await mb.experimentalGetSigningKeyInfo();
    const encrypting = await mb.experimentalGetDeriveKeyInfo();

    const p = await mintblue.envelopes.DocV1.DocV1.create({
      data: pigiv1Test.data,
      options: {
        filename: pigiv1Test.filename,
        mimetype: pigiv1Test.mimetype,
        meta: 'hello',
      },
      signers: [signing.jwk],
      type: 'doc',
      iterations: 200000,
      receivers: [encrypting.uncompressedPublicKey],
    });

    chai.assert.equal(p.info.encryptedData.length > 3, true);

    const parsedWithReceiverKey = await mintblue.envelopes.DocV1.DocV1.parse({
      encryptedData: p.info.encryptedData,
      receiverKey: encrypting.jwk,
    });
    chai.assert.deepEqual(parsedWithReceiverKey.info.data, pigiv1Test.data);
    chai.assert.deepEqual(parsedWithReceiverKey.info.options.filename, pigiv1Test.filename);
    chai.assert.deepEqual(parsedWithReceiverKey.info.options.mimetype, pigiv1Test.mimetype);
  });

  it('jose.generalDecrypt should be able to accept our custom getKey function', async () => {
    // arrange
    const signingKeyPair = await mintblue.utils.crypto.subtle.generateKey(
      {
        name: 'ECDSA',
        namedCurve: 'P-256',
      },
      true,
      ['sign', 'verify'],
    );

    const encryptionKeyPair = await mintblue.utils.crypto.subtle.generateKey(
      {
        name: 'ECDH',
        namedCurve: 'P-256',
      },
      true,
      ['deriveKey'],
    );

    const jwkSigningKey = await mintblue.utils.jose.exportJWK(signingKeyPair.privateKey);
    const jwkEncryptionPublicKey = await mintblue.utils.jose.exportJWK(encryptionKeyPair.publicKey);
    const jwkDecryptionPrivateKey = await mintblue.utils.jose.exportJWK(encryptionKeyPair.privateKey);
    const jwkDecryption = await mintblue.utils.jose.importJWK(jwkDecryptionPrivateKey, 'ECDH-ES');

    // act
    const encryptedEnvelop = await mintblue.envelopes.DocV1.DocV1.create({
      data: pigiv1Test.data,
      options: {
        filename: pigiv1Test.filename,
        mimetype: pigiv1Test.mimetype,
        meta: 'hello',
      },
      type: 'doc',
      iterations: 200000,
      signers: [jwkSigningKey],
      receivers: [jwkEncryptionPublicKey],
    });

    const parsedEnvelop = await mintblue.envelopes.DocV1.DocV1.parse({
      encryptedData: encryptedEnvelop.info.encryptedData,
      receiverKey: (protectedHeader, token) => {
        console.log({ protectedHeader, token });
        return jwkDecryption;
      },
    });

    // assert
    const decodedData = new TextDecoder().decode(parsedEnvelop.info.data);
    chai.assert.equal(decodedData, 'hello');
  });

  it('Doc v1 parse should work by passing SDK users Keys', async () => {
    // arrange
    const mb = await mintblue.Mintblue.create({
      token: SDKUser.token,
      userKeyAttributes: SDKUser.userKeyAttributes,
      mockApi: keyMigrationMockApi,
    });

    const signing = await mb.experimentalGetSigningKeyInfo();
    const encrypting = await mb.experimentalGetDeriveKeyInfo();

    const encryptedEnvelop = await mintblue.envelopes.DocV1.DocV1.create({
      data: pigiv1Test.data,
      options: {
        filename: pigiv1Test.filename,
        mimetype: pigiv1Test.mimetype,
        meta: 'hello-meta',
      },
      type: 'doc',
      iterations: 200000,
      signers: [signing.jwk],
      receivers: [encrypting.uncompressedPublicKey],
    });

    // act
    const joseJWKSGetKey = await mb.keys.asJoseGeneralDecryptGetKey();
    const parsedEnvelop = await mintblue.envelopes.DocV1.DocV1.parse({
      encryptedData: encryptedEnvelop.info.encryptedData,
      receiverKey: joseJWKSGetKey,
    });

    // assert
    const decodedData = new TextDecoder().decode(parsedEnvelop.info.data);
    chai.assert.equal(decodedData, 'hello');
  });

  it('mintblue.parseTransaction should work for a DocV1 TX that has a public key in its receivers which is present as a private key in the reading SDK.', async () => {
    // arrange
    // create 2 mb instances, with their own keyset
    // 1. sender:
    //    - will sign it
    //    - will use a public key from receiver to send its DocV1 to
    // 2. receiver:
    //    - has in its keyset the corresponding private key (see sender)

    const { sdk: mbSender } = await createSDKWithMockApi(SDKUser);
    const { sdk: mbReceiver } = await createSDKWithMockApi(SDKUser2);

    // add a random key to mbReceiver
    const jwkReceiver = await createRandomECDHKeyAsJWK();
    await mbReceiver.keys.import(
      'docv1test_5-doc.test.js',
      mintblue.utils.base64.base64ToBuf(jwkReceiver.private.d),
      mintblue.SupportedKeyDerivationTypes.P256_ECDH,
    );

    // create docv1 message and use signer (mbSender) and receiver (mbReceiver)s keys
    const docv1 = {
      ...docv1Test,
      data: new TextEncoder().encode('data from mbSender'),
      signers: [(await mbSender.experimentalGetSigningKeyInfo()).jwk],
      receivers: [jwkReceiver.public],
    };

    // use mbSender to create a transaction from this docv1
    const { txid, rawtx } = await mbSender
      .createTransaction({ outputs: [docv1] })
      .then(({ outputs }) => outputs[0].value) // extract the only value (= docv1)
      .then(mintblue.utils.createFakeTxFromOutputScript); // convert to a bitcoin rawtx

    // act
    const [docv1parsed] = await mbReceiver.parseTransaction(txid, rawtx);

    // assert
    chai.assert.exists(docv1parsed);
    chai.assert.exists(docv1parsed.data);
    chai.assert.equal(docv1parsed.type, 'doc');
    chai.assert.equal(new TextDecoder().decode(docv1parsed.data), 'data from mbSender');
  });
});

async function createRandomECDHKeyAsJWK() {
  const masterKey = await mintblue.utils.generateSymmetricKey();
  const extraPKey = await new mintblue.P256KeyDerivationStrategy('ECDH').create(masterKey, "m/0'/1'");
  const privateKey = await extraPKey.toJWK({ includePrivate: true });
  const publicKey = await extraPKey.toJWK({ includePrivate: false });
  return { private: privateKey, public: publicKey };
}

async function createSDKWithMockApi({ token, userKeyAttributes }) {
  const derivationsCollection = [];
  const sdk = await mintblue.Mintblue.create({
    token,
    userKeyAttributes,
    mockApi: {
      get: async (url) => {
        return { data: _.keyBy(derivationsCollection, 'fingerprint') };
      },
      post: async (url, data) => {
        if (url.includes('derivation')) {
          derivationsCollection.push(data);
        } else {
          return { data: { data } };
        }
      },
    },
  });
  return { sdk };
}
