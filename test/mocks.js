/* eslint-disable @typescript-eslint/no-unused-vars */
const SDKUser = {
  token: 'secret-token:mintBlue.com/sdk/52e95e1b-5ca3-48f0-bcaf-7f8361ea2967.kRsyL7G9hkOGeu6Epguw',
  userKeyAttributes: {
    masterKeyEncryptedWithKek: 'bzfIKpcWYyA+3whR6td2mj0U7Z7BqJ2ZCXv2B/+PXifF2+2Yiy79L/g7IkHqu6Jza43fzpdJsnheik4X',
    masterKeyEncryptedWithRecoveryKey:
      'ooYmvAJyROCgKRJ0vKwFvP5y4NWG4LTlQB3vL056jaP8nz3kL8lhWETqfVzdpv4oWm2LAHuIpbnTK0gZ',
    recoveryKeyEncryptedWithMasterKey:
      'qaaA6SEUCDH3kTBZoaIoIWdHR3MMylnNogfr91qItpYaZsok/j1Z3SHsUB7RHG4a8Gi+tuxfWfaTuw7v',
    privateKeyEncryptedWithMasterKey:
      '8wnyuXbUmgEImXI/yETQRYLDi08DmdlLc/3S5hc8imKzooYIQ1w67O651HagaGQ6GKomjBi3kcb/ZKvFBvc=',
    publicKeyHex: '031d22ac9572f3537d00be17f8fced9b9dfd672d9f623ae7caaa934eb53a5b39e4',
    kekSalt: '0VMcuy3MDY9BRue/1Wqml4DyNPXTeSL5aRLgtT0fPe4=',
    kekIterations: 200000,
  },
  derivationsMap: {
    '21155abdf1469b9ad887f58700ece803b987a405e44dab331e372d07093c4643': {
      id: '9cf5e3ba-845b-4951-94f9-9607fa8f2cd1',
      name: 'mintblue_deriving',
      path: 'Phhp83htXFfHVZCgVnhJo/Vs4HVyVRep2jVUUtEWG2Qrku7H0FmjWc3p7U76pTN82jaqv7kESLbNETJWI9UjkKVDtrMGG0orKxFH4nlhT0zns6faxCr36hMrP8y+bIL0lE5nTkYNoLCpWIBRrHm9daq3kDon5UAzLtPEZIe7YWjtt9akeNdYSLPC3u8wBCGuzFjD5UN3fdaJgtLrJ42v5z95bLKe+C+CVG2cSCE5uFoX3mpkwcFTUOP+eeGhCWIzoeCYJoVm8yXCH/I9OMuBei9+dbrUZoyo2v81R6deW4OEsUgyydUdKqdSykmyw5UTvPaMINWRGHcDb+7VFiew0HwlHUslGhHF55vQbJn3o5raA/KkoxP5SYjboycBPJ0El7BHQJAUSYziOjNHqt9mAv0brLAxm4QhvMTTBTQt93a2p7dI+WQhRD+aHOHSDIZT9aS9twjdOjMfG4JflxUmn2WLzhevTfxohxXStu1ADFexEMVlJs59y//0Aj0Htg9ElTq34Q==',
      pubkey:
        '044dcc81a5975b24071d3c87cb9a90d08abbc0f5a625af7b73354253f4b99d54c25c661f61e99929982455d394708e88213298b91b134842683da1dd8558cb5e0e',
    },
    '24adb25d0178248492bed946c4d2b380bd3ebeb5a14be7160a5cf1b670662e07': {
      id: 'cb80f150-3e9f-4647-9234-6d6b3a32c91c',
      name: 'kvk',
      path: '3sXSq1nN5XmZQM62n4pTq/fxPBYxjymn3BIc3hH9GcKgP4mc6oaJdOhfMqi6zx8j5qZLTAIw/kZbOncY7a/btC+DKyGAGq0RTmC07fnh/6nSRpojuFFSKkUeHqmY+Om0atic73TOsTthseEnaRslTvy4fOHya3aWl94c++7TZIEMLHCq8bpV9Q34dAArkpqS8X7+EndlT9V/tzjt58V9r4RK2PFjVWctDaYQAk6kUsODaObeZkCCSvZsLJZZwsMe/lCeQUW9FiylOaK5YeAAleRgxd6jAouyFxc8znRv+ZWbsP5uBk6ifuTYKVUWSEAPvVko8v70A3eij6SNorzafAydt5VKpE74jg96LHAZ6EcaXJUOm7JOozBXrJyqo/ng34HUKhAD02fz5S//iGvnwGDp6HlTW6RKb1aChufycjYVIzB6ghZMF+NgKaJ/Z05hB1KIgOfYwHXEZUIcWCbAUAlhGQIODdxND4auunPyhh6IQKRlDQIIUr4Mll6J+Ll18FyEng==',
      pubkey:
        '04080a562f38cb4d818d35db67b66bb58ace331d6bdbc213b05c2f22677c7f8158aff6a9514be6f39980406ce9f151e529ac55f7d9ee974bf5d8461e024f8da9ca',
    },
    '36c2e4b78477cc70a26956960b247c372eaefcef6c7aa28e96dab5ba8bad0323': {
      id: '8fdb9ec0-bd27-45c7-a825-668e52124f99',
      name: 'mintblue_signing',
      path: 'UE5Uy7kcakjBpWN3l/PJcf7axNBgnQ59I6g1z0euFhOoQl0N9klKup0DgO8hS4NvReKUzgGXDwRbSTABM44fq++LO/dn05QFenZ7Eu+mjyxVZGTjTKhhfBGc2tBOesTCCJ2ZENs0XZy5LnXUJiJ0irU6FHAcwYrdxMd4t/LYROi7s460YnlHZ0VqELfYBw8ZnVdbjAyLFtZn5xSDGH/HA5s4sS1sJs4wTuivcfunyCBkWZo1zXnvZNEzOTZXt0Vvo0CdCCIq9JXCICIogOH97dUlvu0JT6YDhrbpBq2z9J9nyHnN+EmcAzchf1qlSXqrc1+2GbSjtkkiy5QGOZtnwpBdqkbX/MiI//Ey9vVqhQs85EGB81aud57bTt61F+zZOlVX+RnzZHRcebOV6CTr9ZyRaGpr5Hmu078+I6lnvZ5zStRWmKWne1jTdbZCX6gwh0gDRIIbSa2YNxF36E7ukI02O+A3H+0vXjLQYi2mIhPw4f6kVaN0TsrlNGH332ZFZgdZWA==',
      pubkey:
        '04e2cab59d23fc0e264e47a1eebabd7e55ba5e40f49582fe8619d7e555af24cf6f4a028eb3bce28d40345fe3f2e2907739777fe0e0a2fc1edede7716250eca497d',
    },
    '5a5cbdfd24b8bd1a96aa223a36ef171f07495a9c0908c9a3ce20c6ebd4c8efb3': {
      id: '37d717de-25c9-4e75-b4b5-eabcef5dd071',
      name: 'bitcoin',
      path: 'dOcyknASyQAgt7iuuEiivJroeBVzi8ePrtANy1Gusa0iTf5pD/7dlf8vpUB7EQTjFKcDydod0jtq8AkcdvxK7RBBZnQesZtn2oYHu+tdEmeWZrW2d4qankiyHrPVtXPHz4+/g6DJ/mzOQEKbYdYlG/ToStEvw43pJtOnfOf5gCs7bRCZxDVW6lqUWvbuK+dnNqSStOYkRgu2hjSNxaLl2n4rvlIkeC1bDVlsmM4cq4L1otn5laklo1SNGHDsNpMRQZ4trkOUr2AYVzSUHwdSz2tqBAXGmxTJEhRuhLlEiSpIz0Z2XuBH3plZGKhqSDM8j74wS+BUW1SPJgP2BMU5iRmBMTSOzoKCdPuGLe2KjCoAqA5ruXulfqESuTqHYfeRZjnEOsqseT176oTR48dedyh8YqiEYvFPSgXu4hiLb362EHNBpkz/',
      pubkey: '031d22ac9572f3537d00be17f8fced9b9dfd672d9f623ae7caaa934eb53a5b39e4',
    },
    a7307e9dfe5f6263fbccbc8c075410fee9249d98e58ada8744ff0662a134fc33: {
      id: 'c1bbca4e-9aad-4314-9bdc-5a9d2ef6c9ec',
      name: 'peppol',
      path: 'szDOfN+QkNQGrTNC77+ZU56wkEy14NP/BWZZ1l//Wa2i4HPTit04No+vib0E+iS4kDY5yWHZba2HfwZf4p9p8iWX2ekztS2ZBY0aN9SBAAHiADa8zMUj4FhSmN9B1YW9szfy3Ps2nPPbux1tCaYIrEsEjGAZfegRFCD2aMdbZI+E8MbZjiZS3nDms8XhiDwtNhRTybytdle8dN96kOt0JySaUvbI5VdX64DvreX1v6dyV+YYt2SwL0j655FxqzyWlKK6MIzCShRLbdLeB0sG4ZT/5StddzbZo8D0d7RwUo8VL2CGSK7ODTr1PaeLqvtgqhvee38CnMKoAGCrsVnWqmG9PwRmuTz1g2leFHYTNNPpr7vbWnZxnrAGqfgnQ5ANmQLGKX0rq3QhAxoBIkO4Wu0j2qnonENB8C6DZ/tIsW4G2I+JXAuKsj8wjSeZPi/EwgCf6qANmNuuEk5xTCeX8Y2g4j0vZCqBYNsQxu6ikpBzrh4RXfrISWmMJdvFK+OLu4s9Mg==',
      pubkey:
        '04ee5cef796a1a72306d56e71bdbc4cc931d8388eaa13a936cb08ecc5ed9acbb1041725f31067d2916c4c9f9d43ff4edbf0a314c4ad89228dada4a797bf76c675e',
    },
  },
};

const mockApi = (derivationsMap) => ({
  get: async (url) => {
    if (url === '/derivations') {
      const d = _.mapValues(derivationsMap, (value, key) => ({
        ...value,
        public: false,
        user_id: '68c1c3fb-bd8d-480f-a926-40f77c227c6d',
        protected: false,
        account_id: '4dc23b15-59f4-4773-ab3c-b6f8167e1d11',
        created_at: '2024-08-06T12:48:42.375',
        updated_at: '2024-08-06T12:48:42.375',
        fingerprint: key,
      }));

      return {
        data: d,
      };
    }
    chai.assert.fail("This test does a GET call that wasn't expected.");
  },
  post: async (url) => {
    return Promise.resolve();
  },
});

if (typeof window === 'undefined') {
  global.SDKUser = SDKUser;
  global.keyMigrationMockApi = mockApi(SDKUser.derivationsMap);
  global.keyMigrationMockApi2 = (keysMap) => mockApi(keysMap);
} else {
  window.SDKUser = SDKUser;
  window.keyMigrationMockApi = mockApi(SDKUser.derivationsMap);
  window.keyMigrationMockApi2 = (keysMap) => mockApi(keysMap);
}
