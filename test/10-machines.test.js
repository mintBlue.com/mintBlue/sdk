const machineId = 'c5cbc95b-4c32-4cc3-991c-5526de7b0884';
const projectName = '10 machines test-js';

const myMockApi = {
  get: async (url) => {
    if (url === 'project') {
      return {
        data: { data: ['itsa me mario'] },
      };
    } else if (url === 'machines') {
      return {
        data: {
          data: [
            {
              id: machineId,
              name: 'My machine',
              aliasUrl: '/mach',
              members: [],
              projectId: 'project-id-test-1',
            },
          ],
        },
      };
    }
    chai.assert.fail("This test does a GET call that wasn't expected: " + url);
  },
  delete: Promise.resolve,
  post: (url) => {
    if (url === 'project') return { data: { data: { id: 'project-id-test-1' } } };
    if (url === 'machine') return { data: { data: {} } };
    chai.assert.fail("This test does a POST call that wasn't expected: " + url);
  },
};

async function removeTestProjectIfExist(mb) {
  const projects = await mb.listProjects();
  const project = projects.find(({ name }) => name === projectName);
  if (project) {
    await mb.destroyProject({
      id: project.id,
    });
  }
}

describe('machines API', async () => {
  it('should be able to register and list machines', async () => {
    // arrange
    const mb = await createMintblueSDKInstance({ mockApi: myMockApi });
    await removeTestProjectIfExist(mb);
    const project = await mb.createProject({
      name: projectName,
      description: 'this project was created from our SDK integration tests',
    });

    // act
    await mb.registerMachine({
      id: machineId,
      name: 'My machine',
      aliasUrl: '/mach',
      project_id: project.id,
      members: [],
    });
    const machines = await mb.listMachines();

    // assert
    chai.assert.lengthOf(machines, 1);
    chai.assert.deepEqual(_.omit(machines[0], ['createdAt', 'updatedAt']), {
      id: machineId,
      name: 'My machine',
      aliasUrl: '/mach',
      members: [],
      projectId: project.id,
    });
  });
});
