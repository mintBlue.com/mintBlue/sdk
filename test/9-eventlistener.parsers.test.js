const rawtx =
  '0100000002ed221e22f874aafec11a81c3b0b0854d6a212af53bef1de0e73814839f9714a8010000006a47304402205922f88c9f87fb81417d8d77d2b01031017620223e5471815d7b3ffe4b8a02bf02203355ebd2eb95c9240c473cfe289138412efe1dae1372ddca37384bf86e5cdf444121030575c9516aac3ae043188993c6b6b59babb06aad07e81abb38283f89f6aeede4ffffffff34dccc8c8e175c95bd44127883134975229b5cd3a15842ad622a7f80cad6bd1e010000006a47304402206ea1ed718c80a9f02814d2ea4087fe91e4dcc80c19ed40ac59ddcec3cd64316f02203ad77dcff365c59cbbf133d02695e2132eb0cfcc4dfd392f6db723c9fe30d254412103fcda126090896b58e6afcc79f66a5dc16118e374fb02671e4e3b7c11dd0c088effffffff03e8030000000000001976a91428d845e6fc2d7127b0811029b85ec0ccf2ba85cf88acae010000000000001976a914e84510e96e8a1b8efa810e0afbcc8af9f2bd040988ac59000000000000001976a914b0f6bc9c3e1667bbae1c7d06f1040e1e4160dba188ac00000000';

describe('parse listener txs', async () => {
  it('should parse payment to', async () => {
    const sut = mintblue.eventListeners.parseListenerTx.paymentTo;
    const triggerAddress = '14iy7b8xkDe63tqDDcxLmzYccfKU7okDGT';
    const txParsed = await sut(rawtx, triggerAddress);
    chai
      .expect(txParsed.senders)
      .to.have.same.members(['18QULr3ezoL4FuojLSfmCawzP9KBqiwkFv', '1FWZ1NsNYtGdsozAg3ihW55o7EQJZNnaYT']);
    chai.assert.deepEqual(txParsed.receivers, [
      { address: triggerAddress, amount: 1000 },
      { address: '1NB8WJL3nLHWxAZfAXFYXbxnf52wGS1HyE', amount: 430 },
      { address: '1H8hWLJGtWdErtu7GZyWVXgDT9KcGSmrHi', amount: 89 },
    ]);
    chai.assert.equal(txParsed.receivedSatoshis, 1000);
  });

  it('should parse payment from', async () => {
    const sut = mintblue.eventListeners.parseListenerTx.paymentFrom;
    const triggerAddress = '18QULr3ezoL4FuojLSfmCawzP9KBqiwkFv';
    const txParsed = await sut(rawtx, triggerAddress);
    chai.assert.equal(txParsed.sentSatoshis, 1430);
  });
});
