const {
  Buffer: { Buffer },
} = window.ethereumjs;

window.createMintblueSDKInstance = async ({ mockApi }) => {
  return new mintblue.Mintblue({ mockApi, keys: 'mocked_unused' });
};
