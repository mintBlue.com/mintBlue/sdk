//TODO: convert this to JS so browser also runs it

import { assert, expect } from 'chai';
import * as mintblue from '../src/index';
import chaiAsPromised from 'chai-as-promised';
import { createMintblueSDKInstance } from './node/helper';

chai.should();
chai.use(chaiAsPromised);
let mb: mintblue.Mintblue;
const sessionId = Math.random().toString(36).substring(7);

before(async () => {
  mb = await createMintblueSDKInstance();
  assert.exists(mb);
});

describe('Key derivation tests', () => {
  it('AES256GCM - Encrypt, Decrypt', async () => {
    // Arrange
    const masterKey = await mintblue.utils.generateSymmetricKey();
    const strategy = new mintblue.AES256GCMKeyDerivationStrategy();

    // Act
    const key: mintblue.AES256GCMKey = await strategy.create(masterKey, 'Encrypt, Decrypt');

    const plainText = new TextEncoder().encode('Hello, MintBlue!');
    const encrypted = await key.encrypt(plainText);
    const decrypted = await key.decrypt(encrypted);

    // Assert
    assert.notEqual(plainText, encrypted);
    assert.deepEqual(plainText, decrypted);
  });

  // We want to prove that if you derive keys in 2 different contexts, they will equal the same key.
  it('P256 ECDSA - Derive, Sign, Verify', async () => {
    // Arrange
    const masterKey = await mintblue.utils.generateSymmetricKey();
    const strategy = new mintblue.P256KeyDerivationStrategy('ECDSA');
    const salt = "m/0'/1'";

    // Act
    const derivedKey1 = await strategy.create(masterKey, salt);
    const derivedKey2 = await strategy.create(masterKey, salt);
    const derivedKey3 = await strategy.create(masterKey, 'garbage');

    const plainText = new TextEncoder().encode('Hello, MintBlue!');
    const signature = await derivedKey1.sign(plainText);

    const verification = await derivedKey1.verify(signature, plainText);
    const verification2 = await derivedKey2.verify(signature, plainText);

    // Assert
    assert.deepEqual(derivedKey1, derivedKey2, 'Key 1 and Key 2 were not equal, but they should have been');
    assert.notDeepEqual(derivedKey1, derivedKey3, 'Key 1 and Key 3 are equal but they should not be');

    assert.ok(verification);
    assert.ok(verification2);

    expect(
      derivedKey3.verify(signature, plainText).should.be.rejected,
      'Key 3 should not be able to verify a message from the wrong key',
    );
  });

  it('Secp256k1 - Derive, Sign, Verify', async () => {
    // Arrange
    const masterKey = await mintblue.utils.generateSymmetricKey();
    const strategy = new mintblue.Secp256k1KeyDerivationStrategy();
    const salt = "m/0'/1'";

    // Act
    const derivedKey1 = await strategy.create(masterKey, salt);
    const derivedKey2 = await strategy.create(masterKey, salt);
    const derivedKey3 = await strategy.create(masterKey, 'garbage');

    const plainText = new TextEncoder().encode('Hello, MintBlue!');
    const signature = await derivedKey1.sign(plainText);

    const verification = await derivedKey1.verify(signature, plainText);
    const verification2 = await derivedKey2.verify(signature, plainText);

    // Assert
    assert.deepEqual(derivedKey1, derivedKey2, 'Key 1 and Key 2 were not equal, but they should have been');
    assert.notDeepEqual(derivedKey1, derivedKey3, 'Key 1 and Key 3 are equal but they should not be');

    assert.ok(verification);
    assert.ok(verification2);

    expect(
      derivedKey3.verify(signature, plainText).should.be.rejected,
      'Key 3 should not be able to verify a message from the wrong key',
    );
  });
});

describe('mintBlue key integration tests', () => {
  it('Can create a P256 key and then recover it from the key module', async () => {
    // Arrange
    const keyName = `${sessionId} p256`;

    // Act
    const key = await mb.keys.create(keyName, mintblue.P256_ECDSA_STRATEGY);

    // Assert
    const foundKey = await mb.keys.findKeyByName(keyName);

    console.log('P256 Keys', {
      keyName,
      key,
      foundKey,
    });

    assert.exists(foundKey);
    assert.deepEqual(
      await key.getFingerprint(),
      await foundKey?.getFingerprint(),
      'Search fingerprint doesnt match key fingerprint',
    );
  });

  it('Can create a P256 ECDH key and then recover it from the key module', async () => {
    // Arrange
    const keyName = `${sessionId} p256 ecdh`;

    // Act
    const key = await mb.keys.create(keyName, mintblue.P256_ECDH_STRATEGY);
    // Assert
    const foundKey = await mb.keys.findKeyByName(keyName);

    assert.exists(foundKey);
    assert.deepEqual(
      await key.getFingerprint(),
      await foundKey?.getFingerprint(),
      'Search fingerprint doesnt match key fingerprint',
    );
  });

  it('Can create a Secp256k1 key and then recover it from the key module', async () => {
    // Arrange
    const keyName = `${sessionId} secp256k1`;

    // Act
    const key = await mb.keys.create(keyName, mintblue.SECP256K1_STRATEGY);
    // Assert
    const foundKey = await mb.keys.findKeyByName(keyName);

    assert.exists(foundKey);
    assert.deepEqual(
      await key.getFingerprint(),
      await foundKey?.getFingerprint(),
      'Search fingerprint doesnt match key fingerprint',
    );
  });
});

describe('mintBlue key builder tests', () => {
  it('Can create and recall a created key from the keybuilder', async () => {
    // Arrange
    const keyName = `${sessionId} Asymmetric signing key`;
    const key = await mb.keys.keyBuilder().setSignable().setAsymmetric().setName(keyName).build();

    const foundKey = await mb.keys.findKeyByName(keyName);

    assert.exists(foundKey);
    assert.deepEqual(
      await key.getFingerprint(),
      await foundKey?.getFingerprint(),
      'Search fingerprint doesnt match key fingerprint',
    );
  });
});
