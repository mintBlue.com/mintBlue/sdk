const isRunningInKarma = typeof process === 'undefined';
const envVars = isRunningInKarma ? window.__karma__.config.env : process.env;

const base64 = mintblue.utils.base64;

describe('Key migrations edge cases', () => {
  it('derivation with 31 byte secret should also be able to be rehydrated', async () => {
    // arrange
    const brokenKeyFingerprint = 'ef806d5e53694673691fcc5bdb3d9f5a4f467574585eec26fbaea411b383847b';

    const specificKeys = {
      ef806d5e53694673691fcc5bdb3d9f5a4f467574585eec26fbaea411b383847b: {
        id: 'b41dc52b-b210-4f41-9ac4-d318daabb9e1',
        name: 'P256 ECDSA key with 31bit secret',
        path: 'O/6T2MmOnTjGM0fyQB0L+1gKvsfyiBYE457TCLyEmGbXzVVUS0EO6Up7++qujVUTz9hJrCquNTkiCCrqblvyL+yYlmFxnNUh/3W/ySJOEunD40G50JvlF3WGGZ5FAOuSQpGbVgOJ2Tct44/tYRgqhYRLMO4A93exjqij1/ZskVhpXaO7V8BezddWdX/OFxowv0UpXeAeeoSil7X2iQibB4/MIvTm8BX7xh0hCKwifIbZwctv7yjqy+bwiKLpiDknZFWRkiYrjS1pzdmZmUzQHWXNqcWAbikR5BKlo4yYZBX/jmy3eQLxtizpBFbiLbgYgsJMvTuzFNs4yAVeMVe2Qgd+mFieAOckTBNUpI0UN2zUXIMmrfWlqHlW1AhjacH5bClMfKoRVsjRrWNN+82kiG2w9ybpVT5ZRFVobzrsjlCuQ/d5VewgLl6QGD3fELR504XE7zdQJFwqnc4H91oPr0+sHTGHQKr4rC9Ujhdpk6JEiEi02GBmbJHb37cameJuj41JTawZw/9OoyiqgTco3zZG0l9t6ZzrrYnSXs0hTTA=',
        pubkey:
          '04e8b9541ea424f1b75b080b940e5d48bc0ef97a73b7c87de4fd8b8105aca15ede0e33bc0d30d250f2f22bdcfc86ea550712bcbca520e6c7f4358d7557952e01d6',
      },
      '839f024a9a5e4b2da9f8208f200ee9fb6631474dfe5b88f65a6bbacd5cc79326': {
        id: '0cafa054-5a8b-4b8c-97bc-796ea2554d8b',
        name: 'P256 ECDSA normal working key',
        path: 'WTJjJqIG1Wgapfnl7xIbILUmMijEQcp9BXArMTimyB1DrvK7T+7p4t1WaJntZuRghpvbLIQqowAFPgGSzIjAUI5ezkSBxwkl9eAe/kH/Oujba6wU/ubh0Q6Q31vUnsW44YKgsyoYLeeJl042aZhoAcNaTGkaqgOHcC8Be9oR1Omi9kgH/XkkEfdALtECoxDkBA5/BeJjUbKRg7X/vwsWNhXzxElM5Q8t+CDjmyIPoamuU0lKkA1jMJ4V9EOQw0IPL0+aRitobMU56LBXbjmAmlwcVRwab3k388Jsa6gi/kBsA5aH+NL5p2V31fcjsKhf9yD+8H6OjLz+CGVXshycqljAzj7jPjgUmQyrL+qvbtAjhuxaryyifKd5wOOoO4X4u6z4L7cWcLiGXHTzO5sIucZYYN88xdZRedc1u/9yuEcNGuwbijS52fAbP9TV7WWVc4dsVgf2VN7Yyi5Q3wxIaWnjnRV+wbJ6sfrzRnrbZ664X0RlrXbffdh4geD63Fm0wEo1x2WuK09cY4DlEYkxahlQLcOWG1ukec5ZecwTiFQ=',
        pubkey:
          '04b59572a02e133487444a16fc199c81537bcef37eabaa25587f433d29e2751c3f51a33c3c1dd439c361de55f51be6020e7c087f6490cc678e7cc71a41e3fca2e8',
      },
    };

    // act
    const mb = await mintblue.Mintblue.create({
      token: SDKUser.token,
      userKeyAttributes: SDKUser.userKeyAttributes,
      mockApi: keyMigrationMockApi2(specificKeys),
    });

    // assert
    const keyMap = await mb.keys.getKeys();
    const hydratedKey = keyMap.get(brokenKeyFingerprint);
    chai.assert.equal(hydratedKey.name, specificKeys[brokenKeyFingerprint].name);
    chai.assert.equal(hydratedKey.key.jwk.crv, 'P-256');
  });

  it('should be able to parse random P-256 ECDSA keys', async () => {
    for (let i = 0; i < 1000; i++) {
      // arrange
      const masterKey = await mintblue.utils.generateSymmetricKey();
      const randomNonce = await mintblue.utils.generateSaltToDeriveKey();
      const { secret, xs, ys } = await mintblue.utils.generateP256Key(masterKey, randomNonce);

      // act
      const jwk = {
        kty: 'EC',
        crv: 'P-256',
        d: base64.bufToUrlBase64(secret),
        x: base64.bufToUrlBase64(xs),
        y: base64.bufToUrlBase64(ys),
      };

      let cryptoKey;
      try {
        cryptoKey = await mintblue.utils.crypto.subtle.importKey(
          'jwk',
          jwk,
          { name: 'ECDSA', namedCurve: 'P-256' },
          true,
          ['sign'],
        );
      } catch (e) {
        console.error('Error: importKey failed for jwk', JSON.stringify(jwk), e);
      }

      // assert
      chai.assert.exists(cryptoKey);
      chai.assert.equal(cryptoKey.constructor.name, 'CryptoKey');
    }
  });
});

describe('Key migrations', () => {
  let mb; // sdk client

  before(async () => {
    mb = await mintblue.Mintblue.create({
      token: envVars.MINTBLUE_SDK_TOKEN, // important for decrypting the /derivations
      userKeyAttributes: SDKUser.userKeyAttributes,
      mockApi: keyMigrationMockApi, // /derivations
    });
    chai.assert.exists(mb);
  });

  it('should migrate Bitcoin keys to the key derivation service.', async () => {
    // Arrange
    const keys = await mb.keys.getKeys();

    const bitcoinKey = await mb.keys.findKeyByName('bitcoin');
    console.log('Got keys', { keys, bitcoinKey });

    chai.assert.exists(bitcoinKey, "Couldn't find the bitcoin key.");
    chai.assert.exists(bitcoinKey?.toBitcoinKey(), 'Couldnt convert Secp256k1 key to bitcoin key');

    // const additionalKeys = await utils.generateAdditionalKeys(mb.config.keys!.masterKey);
    const originalBitcoinKey = mb.config.keys.privateKey;

    console.log({
      restoredKey: bitcoinKey.toBitcoinKey(),
      originalKey: originalBitcoinKey,
    });

    // ensure the recreated bitcoin key is the same as what went in.
    chai.assert.deepEqual(bitcoinKey?.toBitcoinKey(), originalBitcoinKey);
  });

  it('should migrate MB_SIGNING_KEY key to the key derivation service.', async () => {
    // Arrange
    const keys = await mb.keys.getKeys();

    console.log('Got keys', { keys });

    const MB_SIGNING_KEYKey = await mb.keys.findKeyByName(mintblue.MB_SIGNING_KEY);

    chai.assert.exists(MB_SIGNING_KEYKey);

    const additionalKeys = await mintblue.utils.generateAdditionalKeys(mb.config.keys.masterKey);
    const oldMB_SIGNING_KEYKey = additionalKeys.find((k) => k.info === mintblue.MB_SIGNING_KEY);

    const importedJWK = await MB_SIGNING_KEYKey?.toJWK({ includePrivate: true });

    console.log({
      restoredKey: importedJWK,
      originalKey: oldMB_SIGNING_KEYKey?.jwk,
    });

    chai.assert.equal(importedJWK?.kty, 'EC');
    chai.assert.equal(importedJWK?.crv, 'P-256');
    chai.assert.equal(importedJWK?.d, oldMB_SIGNING_KEYKey?.jwk.d);
    chai.assert.equal(importedJWK?.x, oldMB_SIGNING_KEYKey?.jwk.x);
    chai.assert.equal(importedJWK?.y, oldMB_SIGNING_KEYKey?.jwk.y);
  });

  it('should migrate MB_DERIVING_KEY key to the key derivation service.', async () => {
    // Arrange
    const keys = await mb.keys.getKeys();

    console.log('Got keys', { keys });

    const MB_DERIVING_KEYKey = await mb.keys.findKeyByName(mintblue.MB_DERIVING_KEY);

    chai.assert.exists(MB_DERIVING_KEYKey);

    const additionalKeys = await mintblue.utils.generateAdditionalKeys(mb.config.keys.masterKey);
    const oldMB_DERIVING_KEYKey = additionalKeys.find((k) => k.info === mintblue.MB_DERIVING_KEY);

    const importedJWK = await MB_DERIVING_KEYKey?.toJWK({ includePrivate: true });

    console.log({
      restoredKey: importedJWK,
      originalKey: oldMB_DERIVING_KEYKey?.jwk,
    });

    chai.assert.equal(importedJWK?.kty, 'EC');
    chai.assert.equal(importedJWK?.crv, 'P-256');
    chai.assert.equal(importedJWK?.d, oldMB_DERIVING_KEYKey?.jwk.d);
    chai.assert.equal(importedJWK?.x, oldMB_DERIVING_KEYKey?.jwk.x);
    chai.assert.equal(importedJWK?.y, oldMB_DERIVING_KEYKey?.jwk.y);
  });

  it('should migrate peppol key to the key derivation service.', async () => {
    // Arrange
    const keys = await mb.keys.getKeys();

    console.log('Got keys', { keys });

    const peppolKey = await mb.keys.findKeyByName('peppol');

    chai.assert.exists(peppolKey);

    const additionalKeys = await mintblue.utils.generateAdditionalKeys(mb.config.keys.masterKey);
    const oldpeppolKey = additionalKeys.find((k) => k.info === 'peppol');

    const importedJWK = await peppolKey?.toJWK({ includePrivate: true });

    console.log({
      restoredKey: importedJWK,
      originalKey: oldpeppolKey?.jwk,
    });

    chai.assert.equal(importedJWK?.kty, 'EC');
    chai.assert.equal(importedJWK?.crv, 'P-256');
    chai.assert.equal(importedJWK?.d, oldpeppolKey?.jwk.d);
    chai.assert.equal(importedJWK?.x, oldpeppolKey?.jwk.x);
    chai.assert.equal(importedJWK?.y, oldpeppolKey?.jwk.y);
  });

  it('should migrate kvk key to the key derivation service.', async () => {
    // Arrange
    const keys = await mb.keys.getKeys();

    console.log('Got keys', { keys });

    const kvkKey = await mb.keys.findKeyByName('kvk');

    chai.assert.exists(kvkKey);

    const additionalKeys = await mintblue.utils.generateAdditionalKeys(mb.config.keys.masterKey);
    const oldkvkKey = additionalKeys.find((k) => k.info === 'kvk');

    const importedJWK = await kvkKey?.toJWK({ includePrivate: true });

    console.log({
      restoredKey: importedJWK,
      originalKey: oldkvkKey?.jwk,
    });

    chai.assert.equal(importedJWK?.kty, 'EC');
    chai.assert.equal(importedJWK?.crv, 'P-256');
    chai.assert.equal(importedJWK?.d, oldkvkKey?.jwk.d);
    chai.assert.equal(importedJWK?.x, oldkvkKey?.jwk.x);
    chai.assert.equal(importedJWK?.y, oldkvkKey?.jwk.y);
  });
});
