// Hashed v1

describe('Hash V1', () => {
  const SDKUser = {
    token: 'secret-token:mintBlue.com/sdk/IzNRBog7jmwC.lD6IR7idilvs5tRY2G7x',
    userKeyAttributes: {
      masterKeyEncryptedWithKek: 'yoflxnUPwMoDpNGQGUH6RpCb6S7YUrl5uYYIO731gzGPTkpxoqEXpvroHpMoS5RKlYu7P9+/HV9DC083',
      masterKeyEncryptedWithRecoveryKey:
        'kTqezl2j3zjWDLWAy6WSh6SuJNI41l3Gs8OpfbZkapdeC97LP4D3krOZJPT0jP3rx+KonElwjkZp19+k',
      recoveryKeyEncryptedWithMasterKey:
        '2kKnyt34BjRHMs09ZiYmcprSgXtgsmnOwj/cay62Wf4HpnC67SkF14+b/BHgNUxAr4mCS2bmGu3ZeUO2',
      privateKeyEncryptedWithMasterKey:
        'k6ahtkOoc3CMrFoxZ5v3L+j+d6mCnzsTyMvDQHrcMn0QrHvWzsBkO4mtqU8FIPVY9Oor/uRiiZDcS3EHp9o=',
      publicKeyHex: '025d24a20480233fb04010a3ae448cfde6757dce1ce02a8ee9a2e9913173db6a73',
      kekSalt: 'g5mJ80ma4seNsdsXCWmCPvsv34WbnCa0XYN4nBGEHh8=',
      kekIterations: 200000,
    },
    peppolUncompressedPublicKey:
      '04313e3ef5a75be037fb7579948a31b2eb44b3e6477187ba5eef65ede8de0ce9219de9138fb156c3d44698510f59725dd9a1731ae27fc3ef32abac34112dd1fbad',
  };

  it('Should construct a valid hashed content from a byte array - SHA-256', async () => {
    const msgUint8 = new TextEncoder().encode('Testing this');
    const options = {
      algorithm: 'SHA-256',
      data: msgUint8,
    };

    const hashed = await mintblue.envelopes.Hash.Hash.create(options);
    chai.expect(hashed.info.algorithm).equals('SHA-256');
    chai.expect(hashed.info.digestHex).equals('a7cca79bdf3c0626114eaac145d78e5360254ff458f73d95c86bbca6894943af');
  });

  it('Should construct a valid hashed content from a byte array - SHA-512', async () => {
    const msgUint8 = new TextEncoder().encode('Testing this');
    const options = {
      algorithm: 'SHA-512',
      data: msgUint8,
    };

    const hashed = await mintblue.envelopes.Hash.Hash.create(options);
    chai.expect(hashed.info.algorithm).equals('SHA-512');
    chai
      .expect(hashed.info.digestHex)
      .equals(
        'fe2ea049a278c8f9f966058d96f63642250e1656faf96c4af7b52db9eae0d460cc00d237fd3651803289475cf669750b5677e50bd7fbd6c93305b47869e90cc1',
      );
  });

  it('Should construct a valid OP_RETURN HEX', async () => {
    const msgUint8 = new TextEncoder().encode('Testing this');
    const options = {
      algorithm: 'SHA-256',
      data: msgUint8,
    };

    const hashed = await mintblue.envelopes.Hash.Hash.create(options);
    chai
      .expect(hashed.toOpReturnScriptHex())
      .equals(
        '006a04554e495620a2646d657461a1647479706564686173686570726f746f686d696e74426c75653ca2666469676573745820a7cca79bdf3c0626114eaac145d78e5360254ff458f73d95c86bbca6894943af69616c676f726974686d675348412d323536',
      );
  });

  it('Should throw an error when requesting sign or encryption without providing a key', async () => {
    const msgUint8 = new TextEncoder().encode('Testing this');

    try {
      await mintblue.envelopes.Hash.Hash.create({
        algorithm: 'SHA-256',
        data: msgUint8,
        sign: true,
      });
      expect(false).equals(true, 'Hash create() is not throwing options error');
    } catch (err) {
      chai.expect(err.message).equals('Invalid options. Please provide a key to sign and/or encrypt.');
    }

    try {
      await mintblue.envelopes.Hash.Hash.create({
        algorithm: 'SHA-256',
        data: msgUint8,
        encrypt: true,
      });
      expect(false).equals(true, 'Hash create() is not throwing options error');
    } catch (err) {
      chai.expect(err.message).equals('Invalid options. Please provide a key to sign and/or encrypt.');
    }
  });

  it('Can create a hash v1 output via createAPIOptions with signature', async () => {
    const mb = await mintblue.Mintblue.create({
      token: SDKUser.token,
      userKeyAttributes: SDKUser.userKeyAttributes,
      url: 'https://testapi.mintblue.com',
    });

    try {
      const apiOptions = await mb.createAPIOptions({
        outputs: [
          {
            type: 'hash',
            data: new TextEncoder().encode('Testing this'),
            algorithm: 'SHA-256',
            sign: true,
          },
        ],
        project_id: '123',
      });

      chai.expect(apiOptions.outputs[0].type).equals('script');
      // check if the protocol id is present: hashv1
      chai.expect(apiOptions.outputs[0].value.includes('68617368')).equals(true);
    } catch (err) {
      chai.expect(true).equals(false, 'Hash create() with sign true failed');
    }
  });

  it('Can create a hash v1 output via createAPIOptions with encryption', async () => {
    const keyPair = await mintblue.utils.crypto.subtle.generateKey(
      {
        name: 'ECDH',
        namedCurve: 'P-256',
      },
      true,
      ['deriveKey'],
    );

    const exportedJWK = await mintblue.utils.crypto.subtle.exportKey('jwk', keyPair.publicKey);

    const mb = await mintblue.Mintblue.create({
      token: SDKUser.token,
      userKeyAttributes: SDKUser.userKeyAttributes,
      url: 'https://testapi.mintblue.com',
    });

    const apiOptions = await mb.createAPIOptions({
      outputs: [
        {
          type: 'hash',
          data: new TextEncoder().encode('Testing this'),
          algorithm: 'SHA-256',
          sign: true,
          encrypt: true,
          pubKey: { x: exportedJWK.x, y: exportedJWK.y, kty: 'EC', crv: 'P-256' },
        },
      ],
      project_id: '123',
    });

    chai.assert.equal(apiOptions.outputs[0].type, 'script');
    // check if the protocol id is present: hashv1
    chai.assert.equal(apiOptions.outputs[0].value.includes('68617368'), true);
  });
});
