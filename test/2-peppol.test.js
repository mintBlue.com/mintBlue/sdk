const testData = {
  extra: Buffer.from('extra data'),
  data: Buffer.from('Hello'),
};

describe('Peppol SHARED', () => {
  it('Can create and parse peppol v1 (no signature)', async () => {
    // Create KeyPair
    const keyPair = await mintblue.utils.crypto.subtle.generateKey(
      {
        name: 'ECDH',
        namedCurve: 'P-256',
      },
      true,
      ['deriveKey'],
    );

    const pubKeyJWK = await mintblue.utils.jose.exportJWK(keyPair.publicKey);
    const uncompressedPublicKey = await mintblue.utils.JWKToUncompressedHex(pubKeyJWK);

    const p = await mintblue.envelopes.PeppolV1.PeppolV1.create({
      pubKey: uncompressedPublicKey,
      type: 'peppol',
      value: testData.data,
      version: 1,
      options: {
        extra: testData.extra,
      },
    });

    chai.assert.hasAnyKeys(p.info, ['encryptedData']);

    const script = p.toOpReturnScriptHex();

    const parsed = await mintblue.envelopes.PeppolV1.PeppolV1.fromOpReturnScriptHex(script, {
      privateKey: await mintblue.utils.jose.exportJWK(keyPair.privateKey),
    });

    chai.assert.deepEqual(parsed.info.value, testData.data);
    chai.assert.deepEqual(parsed.info.options.extra, testData.extra);
  });

  it('Can create and parse peppol v1 (with signature, no verify)', async () => {
    // Create KeyPair
    const keyPair = await mintblue.utils.crypto.subtle.generateKey(
      {
        name: 'ECDH',
        namedCurve: 'P-256',
      },
      true,
      ['deriveKey'],
    );

    // Create keyPair for signing
    const signKeyPair = await mintblue.utils.crypto.subtle.generateKey(
      {
        name: 'ECDSA',
        namedCurve: 'P-256',
      },
      true,
      ['sign', 'verify'],
    );

    const pubKeyJWK = await mintblue.utils.jose.exportJWK(keyPair.publicKey);
    const uncompressedPublicKey = await mintblue.utils.JWKToUncompressedHex(pubKeyJWK);

    const p = await mintblue.envelopes.PeppolV1.PeppolV1.create({
      pubKey: uncompressedPublicKey,
      type: 'peppol',
      value: testData.data,
      version: 1,
      options: {
        extra: testData.extra,
      },
      signingKey: await mintblue.utils.jose.exportJWK(signKeyPair.privateKey),
    });

    chai.assert.hasAnyKeys(p.info, ['encryptedData']);

    const script = p.toOpReturnScriptHex();

    const parsed = await mintblue.envelopes.PeppolV1.PeppolV1.fromOpReturnScriptHex(script, {
      privateKey: await mintblue.utils.jose.exportJWK(keyPair.privateKey),
    });

    chai.assert.deepEqual(parsed.info.value, testData.data);
    chai.assert.deepEqual(parsed.info.options.extra, testData.extra);
  });

  it('Can create and parse peppol v1 (with signature, with verify)', async () => {
    // Create KeyPair
    const keyPair = await mintblue.utils.crypto.subtle.generateKey(
      {
        name: 'ECDH',
        namedCurve: 'P-256',
      },
      true,
      ['deriveKey'],
    );

    // Create keyPair for signing
    const signKeyPair = await mintblue.utils.crypto.subtle.generateKey(
      {
        name: 'ECDSA',
        namedCurve: 'P-256',
      },
      true,
      ['sign', 'verify'],
    );

    const pubKeyJWK = await mintblue.utils.jose.exportJWK(keyPair.publicKey);
    const uncompressedPublicKey = await mintblue.utils.JWKToUncompressedHex(pubKeyJWK);

    const p = await mintblue.envelopes.PeppolV1.PeppolV1.create({
      pubKey: uncompressedPublicKey,
      type: 'peppol',
      value: testData.data,
      version: 1,
      options: {
        extra: testData.extra,
      },
      signingKey: await mintblue.utils.jose.exportJWK(signKeyPair.privateKey),
    });

    chai.assert.hasAnyKeys(p.info, ['encryptedData']);

    const script = p.toOpReturnScriptHex();

    const parsed = await mintblue.envelopes.PeppolV1.PeppolV1.fromOpReturnScriptHex(script, {
      privateKey: await mintblue.utils.jose.exportJWK(keyPair.privateKey),
      verifyKey: await mintblue.utils.jose.exportJWK(signKeyPair.publicKey),
    });

    chai.assert.deepEqual(parsed.info.value, testData.data);
    chai.assert.deepEqual(parsed.info.options.extra, testData.extra);
  });
});
