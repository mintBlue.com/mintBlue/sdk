describe('Mintblue', () => {
  it('Can create a peppol v1 output (via createAPIOptions, using jwk)', async () => {
    const keyPair = await mintblue.utils.crypto.subtle.generateKey(
      {
        name: 'ECDH',
        namedCurve: 'P-256',
      },
      true,
      ['deriveKey'],
    );

    const exportedJWK = await mintblue.utils.crypto.subtle.exportKey('jwk', keyPair.publicKey);

    const mb = await mintblue.Mintblue.create({
      token: SDKUser.token,
      userKeyAttributes: SDKUser.userKeyAttributes,
      url: 'https://testapi.mintblue.com',
    });

    const apiOptions = await mb.createAPIOptions({
      outputs: [
        {
          type: 'peppol',
          version: 1,
          value: Buffer.from('hello'),
          pubKey: { x: exportedJWK.x, y: exportedJWK.y, kty: 'EC', crv: 'P-256' },
          //signingKey: signingKey,
          options: {
            extra: Buffer.from('hello extra'),
          },
        },
      ],
      project_id: '123',
    });

    chai.assert.equal(apiOptions.project_id, '123');
    chai.assert.equal(apiOptions.outputs[0].type, 'script');
    // check if the protocol id is present: mB:peppol
    chai.assert.equal(apiOptions.outputs[0].value.includes('6d423a706570706f6c'), true);
  });

  it('Can create a peppol v1 output (via createAPIOptions, using uncompress hex key, with signature)', async () => {
    const signingKeyPair = await mintblue.utils.crypto.subtle.generateKey(
      {
        name: 'ECDSA',
        namedCurve: 'P-256',
      },
      true,
      ['sign', 'verify'],
    );

    const signingKey = await mintblue.utils.crypto.subtle.exportKey('jwk', signingKeyPair.privateKey);

    const keyPair = await mintblue.utils.crypto.subtle.generateKey(
      {
        name: 'ECDH',
        namedCurve: 'P-256',
      },
      true,
      ['deriveKey'],
    );

    const exportedJWK = await mintblue.utils.crypto.subtle.exportKey('jwk', keyPair.publicKey);

    const hexKey =
      '04' +
      mintblue.utils.hex.bufToHex(mintblue.utils.base64.base64ToBuf(exportedJWK.x)) +
      mintblue.utils.hex.bufToHex(mintblue.utils.base64.base64ToBuf(exportedJWK.y));

    const mb = await mintblue.Mintblue.create({
      token: SDKUser.token,
      userKeyAttributes: SDKUser.userKeyAttributes,
      url: 'https://testapi.mintblue.com',
    });

    const apiOptions = await mb.createAPIOptions({
      outputs: [
        {
          type: 'peppol',
          version: 1,
          value: Buffer.from('hello'),
          pubKey: hexKey,
          signingKey: signingKey,
          options: {
            extra: Buffer.from('hello extra'),
          },
        },
      ],
      project_id: '123',
    });

    chai.assert.equal(apiOptions.project_id, '123');
    chai.assert.equal(apiOptions.outputs[0].type, 'script');
    // check if the protocol id is present: mB:peppol
    chai.assert.equal(apiOptions.outputs[0].value.includes('6d423a706570706f6c'), true);
  });

  it('Can create a pigi v2 output (via createAPIOptions)', async () => {
    const keyPair = await mintblue.utils.crypto.subtle.generateKey(
      {
        name: 'ECDSA',
        namedCurve: 'P-256',
      },
      true,
      ['sign', 'verify'],
    );

    const signingKey = await mintblue.utils.crypto.subtle.exportKey('jwk', keyPair.privateKey);

    const mb = await mintblue.Mintblue.create({
      token: SDKUser.token,
      userKeyAttributes: SDKUser.userKeyAttributes,
      url: 'https://testapi.mintblue.com',
    });

    const apiOptions = await mb.createAPIOptions({
      outputs: [
        {
          type: 'pigi',
          version: 2,
          value: Buffer.from('hello'),
          signingKey: signingKey,
          options: {
            filename: 'hello.txt',
            mimetype: 'text',
          },
        },
      ],
      project_id: '123',
    });

    chai.assert.equal(apiOptions.project_id, '123');
    chai.assert.equal(apiOptions.outputs[0].type, 'script');
    // check if the protocol id is present: mB:pigi2
    chai.assert.equal(apiOptions.outputs[0].value.includes('6d423a7069676932'), true);
  });

  it('Can create a pigi v2 output (via mock api)', async () => {
    const keyPair = await mintblue.utils.crypto.subtle.generateKey(
      {
        name: 'ECDSA',
        namedCurve: 'P-256',
      },
      true,
      ['sign', 'verify'],
    );

    const signingKey = await mintblue.utils.crypto.subtle.exportKey('jwk', keyPair.privateKey);

    const mockApi = {
      ...keyMigrationMockApi,
      post: async (url, data) => {
        chai.assert.equal(url, 'transaction');
        chai.assert.equal(data.outputs[0].type, 'script');
        chai.assert.equal(data.outputs[0].encoding, 'hex');
        chai.assert.equal(data.outputs[0].value.length > 10, true);
        return { data: { data: 'result' } };
      },
    };

    const mb = await mintblue.Mintblue.create({
      token: SDKUser.token,
      userKeyAttributes: SDKUser.userKeyAttributes,
      mockApi: mockApi,
    });

    const res = await mb.createTransaction({
      outputs: [
        {
          type: 'pigi',
          version: 2,
          value: Buffer.from('hello'),
          signingKey: signingKey,
          options: {
            filename: 'hello.txt',
            mimetype: 'text',
          },
        },
      ],
      project_id: '123',
    });

    chai.assert.equal(res, 'result');
  });

  it('Can output a Peppol public key', async () => {
    const peppolUncompressedPublicKey = _.find(SDKUser.derivationsMap, _.matchesProperty('name', 'peppol')).pubkey;

    const mb = await mintblue.Mintblue.create({
      token: SDKUser.token,
      userKeyAttributes: SDKUser.userKeyAttributes,
      url: 'https://testapi.mintblue.com',
      mockApi: keyMigrationMockApi,
    });

    const keyInfo = await mb.experimentalGetPeppolKeyInfo();

    chai.assert.equal(keyInfo.uncompressedPublicKey, peppolUncompressedPublicKey);
  });
});
