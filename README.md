# mintBlue

[mintBlue](https://mintblue.com/) is here to make your life easy. Want to use blockchain features but don't have the time or expertise to build a blockchain product from scratch? No problem!

This SDK allows you to easily integrate the mintBlue API. It also contains the tools needed to do cryptographic operations, using your [non-custodial keys](https://docs.mintblue.com/sdk-encryption), allowing you to effectively and securely own your own data. Hassle-free and easy to use!

Full API and SDK documentation at [docs.mintblue.com](https://docs.mintblue.com)

## API Reference

[https://mintblue.gitlab.io/sdk/](https://mintblue.gitlab.io/sdk/)

## Features

- Create blockchain transactions
- Encrypt and digitally sign payloads locally
- Fetch transactions from the blockchain and decrypt payloads locally
- Organise your transactions into projects
- Create EventListeners to collect transactions into projects
- Stream your project to your local machine
- Create State Machines from blockchain transactions (see [example](examples/machine))

## Getting started

1. Create an account at [https://console.mintblue.com](https://console.mintblue.com)

2. Create a project and copy the project ID

3. Create an SDK token and copy the SDK token

4. npm install @mintblue/sdk

5. Publish your first transaction via the SDK

```js
import { Mintblue } from '@mintblue/sdk';

(async () => {
  const token = 'YOUR SDK TOKEN';
  const project_id = 'YOUR PROJECT ID';

  const client = await Mintblue.create({ token: token });

  const outputs = [
    {
      type: 'data',
      value: 'Hello world',
      sign: true,
      encrypt: true,
    },
  ];

  const { txid, rawtx } = await client.createTransaction({ project_id, outputs });

  console.log(txid);
})();
```
