module.exports = {
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaVersion: 2018,
    sourceType: 'module',
  },
  plugins: ['@typescript-eslint', 'prettier'],
  env: {
    browser: true,
    node: true,
    mocha: true,
    es6: true,
  },
  extends: ['plugin:@typescript-eslint/recommended', 'plugin:prettier/recommended'],
  rules: {
    'prettier/prettier': 'error',
    // Allow snake_case for properties, enforce camelCase for variables
    // '@typescript-eslint/camelcase': [2, { properties: 'never' }],
    '@typescript-eslint/no-unused-vars': [1, { argsIgnorePattern: '^_' }],
    '@typescript-eslint/interface-name-prefix': [0],
    '@typescript-eslint/no-explicit-any': [0],
    'no-prototype-builtins': [0],
  },
};
