/* eslint-disable @typescript-eslint/no-var-requires */
const webpack = require('webpack');
const NodePolyfillPlugin = require('node-polyfill-webpack-plugin');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const { SubresourceIntegrityPlugin } = require('webpack-subresource-integrity');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const StrictCspHtmlWebpackPlugin = require('strict-csp-html-webpack-plugin');
const { DuplicatesPlugin } = require('inspectpack/plugin');
const path = require('path');
const PACKAGE = require('./package.json');
const version = PACKAGE.version;

module.exports = {
  entry: './dist/browser/index.js',
  devtool: 'source-map',
  output: {
    path: path.resolve(__dirname, 'dist', 'browser'),
    filename: `index.umd.js`,
    library: 'mintblue',
    libraryTarget: 'umd',
  },
  optimization: {
    // minimize: false,
    /* // turn this on if you want to inspect node_module includes
    splitChunks: {
      cacheGroups: {
        vendors: {
          test: /[\\/]node_modules[\\/]/,
          name: 'vendor',
          chunks: 'all',
          enforce: true,
        },
      },
    },
  */
  },
  target: ['browserslist'],
  resolve: {
    fallback: {
      fs: false,
      buffer: require.resolve('buffer/'),
    },
    alias: {
      bsv: '@ts-bitcoin/core',
    },
    extensions: ['.ts', '.js'],
    extensionAlias: {
      '.js': ['.js', '.ts'],
    },
  },
  experiments: {
    topLevelAwait: true,
  },
  module: {
    rules: [
      // {
      //   test: /\.ts$/,
      //   exclude: /node_modules/,
      //   use: {
      //     loader: 'ts-loader',
      //     options: { configFile: './tsconfig/browser.json' },
      //   },
      // },
    ],
  },
  plugins: [
    // new BundleAnalyzerPlugin(),
    new HtmlWebpackPlugin(
      Object.assign(
        {},
        // ... HtmlWebpackPlugin config
      ),
    ),
    new StrictCspHtmlWebpackPlugin(HtmlWebpackPlugin),
    new DuplicatesPlugin({ verbose: true }),
    new NodePolyfillPlugin({ excludeAliases: ['console', 'fs'] }),
    new webpack.ProvidePlugin({
      Buffer: ['buffer', 'Buffer'],
    }),
    new webpack.NormalModuleReplacementPlugin(/node:/, (resource) => {
      const mod = resource.request.replace(/^node:/, '');
      switch (mod) {
        case 'buffer':
          resource.request = 'buffer';
          break;
        case 'stream':
          resource.request = 'readable-stream';
          break;
        default:
          throw new Error(`Not found ${mod}`);
      }
    }),
  ],
};
