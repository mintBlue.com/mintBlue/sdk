# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [9.5.1] 2025-03-03

Added extra parameter `snapshotName` to MachineDto.

## [9.5.0] 2025-02-27

Added 2 methods for handling Machines that a user can store and retrieve from mintBlue
- `registerMachines` allows the user to create/update a new Machine
- `listMachines` will show Machines which the user owns

## [9.4.0] 2025-02-13

Added parsers from event listener payload. Helpful for e.g. Rollout.

## [9.3.2] 2025-02-05

Performance improvement for Machines:
- added a configurable `chunkSize` which makes the Machine yield to Node Event loop after `chunkSize` amount of transactions.
- improved error messages

## [9.3.0] 2025-02-04

- Recorders no longer receive deepCloned copies of snapshots. Recorders should not manipulate received snapshots directly.

## [9.1.0] 2024-10-11

### Added

- Add support for automatic decryption key selection for DocV1 envelopes in parseTransaction()

## [9.0.2] 2024-09-27

### Changed

- Fix in `mb.keys` which affected the rehydration of a small number of p256 ecdsa keys

## [9.0.1] 2024-08-26

### Changed

- A doc issue preventing release of docs has been fixed

## [9.0.0] 2024-08-23

### Added

- the sdk now has a new module available: `mb.keys`!
- this Key Management module allows for creating keys with a certain strategy, derived from your master key
- create them manually with the `mb.keys.createKey` function, or use the KeyBuilder helper
- Keys are saved under your account and linked to the current access token

### Changed

- The mintblue console (console.mintblue.com) also allows to create access tokens and select the available keys

## [8.2.0] 2024-06-07

### Added

- Added txo.rawTxToTxo() function to parse a rawtx into a TXO object

## [8.1.0] 2024-05-30

### Added

- (Machines) Allow the definition of optional `queries` property in App

## [8.0.1] 2024-05-17

### Fixed

- Fixed a bug occuring when installing the package where `dist/cjs/bin/cli.js` could not be found.

## [8.0.0] 2024-05-13

### Added

- Machine Reader classes must now also implement a fetch() method, to fetch transactions in a fire-and-forget manner

### Fixed

- Fixed a bug in the types to create an event listener (insert-into-project became insert_into_project)

## [7.0.0] 2024-04-09

### Changed

- The properties encrypt and sign of the FileOutput, DataOutput, HashOutput are now defaulting to true
- The FileOutput now has a 'fileName' field
- The DataOutput's value field now has type 'any'

## [6.0.1] 2024-02-09

### Changed

- Use backend API v3 by default

## [6.0.0] 2024-02-05

### Changed

- The listTransactions method now accepts limit and offset directly, instead of requiring a paginationOptions object

## [5.2.5] 2023-12-19

### Fixed

- Websocket connection in experimentalStreamProject will now automatically reconnect and keep the last-event-id in mind

## [5.2.4] 2023-12-19

### Fixed

- Websocket connection in experimentalStreamProject will now automatically reconnect and keep the last-event-id in mind

## [5.2.3] 2023-10-19

### Fixed

- Import issues with p-queue

## [5.2.2] 2023-10-13

### Fixed

- Import issues with p-queue

## [5.2.1] 2023-10-03

### Fixed

- Import issues with p-queue

## [5.2.0] 2023-09-29

### Added

- ASM and base64 encoding support for script values

## [5.1.0] 2023-09-28

## Added

- queryTransactions() is a new function that enables detailed querying for transactions, including querying based on earlier provided metadata

## [5.0.1] 2023-09-18

## Added

- Reintroduced login() function, to instantiate SDK with email and password

## [5.0.0] 2023-09-14

## Breaking change

getTransaction() now returns a TransactionWithRawTx instead of parsed outputs. To include the parsed outputs set { parse: true } in the function arguments.
The parsed outputs will then be returned in the 'outputs' field of the result.

## [4.4.6] 2023-09-11

## Added

- React hook is now importable from '@mintblue/sdk/react'

## [4.4.4] 2023-09-08

## Removed

- React hook: useMintblue() is no longer exported by default.

## [4.4.0] 2023-08-31

## Added

- React hook: useMintblue()

## [4.3.0] 2023-08-31

### Added

- Feature: State Machines (beta)
  - See example: [https://gitlab.com/mintBlue/sdk/-/tree/master/examples/machine](https://gitlab.com/mintBlue/sdk/-/tree/master/examples/machine?ref_type=heads)
- New SDK functions:
  - createEventListener
  - getEventListener
  - listEventListeners

### Improved

- Types
- Typedoc documentation (see [https://mintblue.gitlab.io/sdk/](https://mintblue.gitlab.io/sdk/)

### Removed

- Mintblue::login() was removed

## [4.2.1] 2023-08-25

- Reimplemented experimentalStreamProject() to use WebSocket intead of Server-Sent Events

## [4.2.0] 2023-08-24

### Added

- New experimental feature: experimentalStreamProject()

## [4.0.1] 2023-08-15

### Fixed

- Fixed a bug that caused the rawtx option of the createTransaction to not be forwarded

## [4.0.0] 2023-08-09

### Changed

Functions which have more than one argument have been reworked to only use 1 argument in the form of an object with named parameters.
Functions that already had single arguments in the form of an object are not affected. (createTransaction)

For example:

In 3.x getTransaction( txid, secret )

in 4.x getTransaction( { txid, secret } )

## [3.0.3] - 2022-05-18

### Fixed

- Improvements to Hash Output type

### Depcrecated

- version 3.0.2 has been deprecated

## [3.0.2] - 2022-05-17

No changes

## [3.0.1] - 2022-05-17

### Deprecated

- Version 3.0.0 has been deprecated in npm

### Fixed

- HashOutput protocol id is now mB:hash1

## [3.0.0] - 2022-05-17

### Changed

- DocV1Output now has type: 'doc'
- HashOutput now has type: 'hash'

## [2.0.0] - 2022-05-11

### Added

- Hash Envelope: Convenient wrapper to save file hashes on chain without saving the entire file

### Changed

- Uses mintBlue API v2 by default.

## [1.4.0] - 2022-05-02

### Fixed

- Improved packaging:
  - commonjs and esm version for node runtimes
  - esm version and umd version for browser

### Removed

- Pigi V1 envelope

## [1.3.10] - 2022-04-28

Browser build is now included in npm package

## [1.3.9] - 2022-04-26

No changes

## [1.3.8] - 2022-04-26

### Fixed

- CommonJS build is now included in npm package

## [1.3.7] - 2022-04-26

### Fixed

- CommonJS is now supported again

## [1.3.5] - 2022-04-21

No changes

## [1.3.4] - 2022-04-21

### Fixed

- Fixed main entrypoint in package json

## [1.3.3] - 2022-04-21

### Added

- All types are now exported from the main entrypoint

## [1.3.2] - 2022-04-20

### Fixed

- Fixed an issue with importing the library in commonjs format

## [1.3.1] - 2022-04-20

### Fixed

- Fixed an issue with importing the library in commonjs format

## [1.3.0] - 2022-04-20

### Added

- Browser builds with webpack
- Doc v1 envelope type
- Shared browser/nodejs tests

### Fixed

- Various bugs in browser related to envelopes

## [1.2.6] - 2022-03-30

### Fixed

- Fixed a padding issue with uncompressed keys

### Added

- Added TransactionStatus type

## [1.2.6] - 2022-03-30

### Fixed

- Fixed issue causing listTransactions to fail when no dates were supplied in the options

## [1.2.5] - 2022-03-29

### Fixed

- Fixed issue when creating Peppol v1 output via createTransaction

## [1.2.4] - 2022-03-29

### Fixed

- Fixed issue when creating Peppol v1 output

## [1.2.3] - 2022-03-28

### Fixed

- Some Transaction fields were Date but are actually ISO Strings

## [1.2.2] - 2022-03-28

### Fixed

- Improve typings of listTransactions and createTransaction

## [1.2.1] - 2022-12-08

### Added

- Alternative Key fingerprint using SHA1 for KVK

## [1.2.0] - 2022-12-07

### Fixed

- Improve types

### Added

- Support for compressed keys in Peppol outputs
- experimentalGetKeyInfo functions now also return compressedPublicKey, jwk, kid and info

## [1.1.0] - 2022-10-12

### Added

- recoverUserKeyAttributes() regenerates userKeyAttributes from a recovery key and a new password

## [1.0.8] - 2022-10-12

No significant changes

Added experimental KVK key

## [1.0.7] - 2022-10-12

Updated README

# [1.0.6] - 2022-10-12

Version bump to keep in sync with @mintblue/sdk-server

## [1.0.5] - 2022-10-12

### Fixed

- Decryption of Peppol outputs in browser
- Peppol output type now correctly returs pubKey after parsing

## [1.0.1] - 2022-09-19

### Fixed

- Missing dependency 'form-data'

## [1.0.0] - 2022-09-13

### Added

- Ability to instantiate Mintblue SDK with SDK token
- utilities related to key management
- Encrypted envelope for Yuki v1, Yuki v2, Peppol v1
- Create projects
- Create transactions
