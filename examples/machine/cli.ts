/* eslint-disable no-var */
import repl from 'repl';
import { main } from './main.js';

main().then(({ app, machine, recorder }) => {
  console.log(`
    This is an example Machine. To use this example, fill in your SDK token and project_id in main.ts
    If you want detailed debugging logs: DEBUG=mintmachine:* npm run example-cli

    Try the following commands:
  - await machine.loadsnapShot()
  - await machine.start()
  - await app.plantTrees("Belgium", 1)
  - app.state
  - await app.addInformation(0, { color: "green" })
  - app.state
  - await recorder.getSnapshots()
  - machine.createSnapshot();

  `);

  const server = repl.start({
    prompt: 'example > ',
    useGlobal: true,
  });

  server.context.app = app;
  server.context.machine = machine;
  server.context.recorder = recorder;
});
