import { App, Middleware, logger, Context } from '../../src/index.js';

/**
 * The interface for the state of the Forest app.
 */
interface ForestState {
  trees: { location: string; info?: Record<string, any> }[];
  estimatedCarbonOffset: number;
}

/**
 * Represents a Forest application that keeps track of trees planted
 * and their estimated carbon offset.
 */
export class Forest implements App {
  /**
   * Represents the current state of the forest, containing information
   * about the trees and the estimated carbon offset.
   */
  state: ForestState = {
    trees: [],
    estimatedCarbonOffset: 0,
  };

  queries = {
    getTreeByIndex: ({ index }: { index: number }) => {
      return this.state.trees[index];
    },
  };

  /**
   * Middleware-enriched function to plant trees.
   * @param _ctx - The application context.
   * @param location - The location where the trees are planted.
   * @param amount - The number of trees to plant.
   */
  @Middleware(logger)
  plantTrees(_ctx: Context, location: string, amount: number) {
    for (let i = 0; i < amount; i++) {
      this.state.trees.push({
        location: location,
      });
    }
    this.state.estimatedCarbonOffset = this._estimateCarbonOffset();
  }

  /**
   * Middleware-enriched function to add additional information to a tree.
   * @param _ctx - The application context.
   * @param treeIndex - The index of the tree to add information to.
   * @param additionalInformation - The additional details to attach to the tree.
   */
  @Middleware(logger)
  addInformation(_ctx: Context, treeIndex: number, info: Record<string, any>) {
    if (treeIndex > this.state.trees.length - 1) {
      throw new Error("The tree you reference doesn't exist");
    }
    this.state.trees[treeIndex].info = {
      ...(this.state.trees[treeIndex].info || {}),
      ...info,
    };
  }

  /**
   * Private function to calculate the estimated carbon offset based
   * on the number of trees in the forest.
   */
  private _estimateCarbonOffset() {
    return this.state.trees.length * 42;
  }
}
