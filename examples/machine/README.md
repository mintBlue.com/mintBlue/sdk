# Machine example application

This is an example Machine application that demonstrates how to interact with the Mintblue SDK to manage a forest and its trees' data. All of the data will be stored forever on the blockchain via mintBlue's SDK. The application can resynchronize all of the data back from the blockchain. You can create snapshots and store your state locally to boot up faster. After a snapshot has been load you can resume synchronizing from the blockchain.

## Time travel debugging

![Time Travel Debugging GIF](ttd.gif)

## ClI interaction

![CLI Interaction](machine-cli.gif)

## Getting Started

1. Register for an account at [console.mintblue.com](https://console.mintblue.com).

2. After registering and logging in, create a new access token by going to your account settings.

3. Copy the SDK token in the format: `"secret-token:mintBlue.com/sdk/XXXXXX.XXXXXXXXXX"`.

4. Clone this repository and install the required dependencies using npm:

   ```sh
   git clone https://gitlab.com/mintBlue/sdk.git
   cd sdk
   npm install
   npm run build
   cd examples/machine
   ```

5. Open the `main.ts` file and replace `'PUT YOUR SDK TOKEN HERE'` with the SDK token you copied earlier.

6. In the `main.ts` file, replace `'PUT YOUR PROJECT ID HERE'` with the ID of the project you want to associate with the application. You can find this ID in the Mintblue console after creating a new project.

7. Run the example CLI by executing the following command:

   ```sh
   npm run example-cli
   ```

## Using the Example Application

Once you have the application running, you can use the following commands in the CLI:

- `await machine.loadsnapShot()`: Load a saved snapshot of the application's state.
- `await machine.start()`: Start the application's processes.
- `await app.plantTrees("Belgium", 1)`: Plant a specified number of trees in a given location.
- `app.state`: View the current state of the forest.
- `await app.addInformation(0, { color: "green" })`: Add additional information to a specific tree.
- `app.state`: View the updated state of the forest.
- `await recorder.getSnapshots()`: Retrieve the list of saved snapshots.
- `machine.createSnapshot()`: Create a snapshot of the current application state.

## Debugging

To enable detailed debugging logs, you can set the `DEBUG` environment variable before running the example CLI:

```sh
DEBUG=mintmachine:* npm run example-cli
```

## Note

Remember that this example application is intended for demonstration purposes and to help you understand how to interact with the Mintblue SDK. Feel free to modify and extend the code to suit your own use case.

For more information and documentation on the Mintblue SDK, visit [docs.mintblue.com](https://docs.mintblue.com).
