import { Machine, MemoryRecorder, MintblueReader, MintblueWriter } from '../../src/index.js';
import { Forest } from './forest.js';

/**
 * Main function to initiate the application.
 *
 * To interact with this application, cd into examples/machine and then run 'npm run cli'
 * @returns An object containing the app and the wrapped instance of ForestApp.
 */
export async function main() {
  const sdkToken = process.env.MINTBLUE_SDK_TOKEN || 'PUT YOUR SDK TOKEN HERE';
  const project_id = process.env.MINTBLUE_PROJECT_ID || 'PUT YOUR PROJECT ID HERE';

  const recorder = new MemoryRecorder();

  const machine = new Machine(
    Forest,
    await MintblueReader.create(sdkToken, project_id),
    await MintblueWriter.create(sdkToken, project_id),
    recorder,
  );

  const app = machine.getInstance();

  machine.on('plantTrees', () => {
    console.log('A tree was planted');
  });

  // machine.loadSnapshot();
  // machine.start();
  // await forest.plantTrees('Belgium', 1);

  return { machine, app, recorder };
}
