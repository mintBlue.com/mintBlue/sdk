/* eslint-disable no-var */
import repl from 'repl';
import * as mb from '../index.js';

if (!process.env.MINTBLUE_SDK_TOKEN || process.env.MINTBLUE_SDK_TOKEN.length < 2) {
  console.log('You must set MINTBLUE_SDK_TOKEN environment variable to use this console');
  process.exit(1);
}

declare global {
  var client: mb.Mintblue;
  var mb: typeof import('../index.js');
}

mb.Mintblue.create({
  token: process.env.MINTBLUE_SDK_TOKEN as string,
  ...(process.env.MINTBLUE_API_URL && { url: process.env.MINTBLUE_API_URL }),
}).then((client) => {
  global.client = client;
  global.mb = mb;
  console.log('Client ready!');
  console.log("Use 'client' and 'mb' variables to interact with mintBlue!");

  repl.start({
    prompt: 'mintBlue > ',
    useGlobal: true,
  });
});
