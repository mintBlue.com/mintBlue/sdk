import lzma from 'lzma/src/lzma_worker.js';
const { LZMA_WORKER: LZMA } = lzma;

// Importing this will create a LZMA variable. See global.d.ts

LZMA.disableEndMark = true;

export async function compress(data: Uint8Array, mode: number): Promise<Uint8Array> {
  return new Promise((resolve, reject) => {
    LZMA.compress(data as unknown as any, mode, (result, error) => {
      if (error) {
        reject(error);
      }
      resolve(new Uint8Array(result as any));
    });
  });
}

export async function decompress(data: Uint8Array): Promise<Uint8Array> {
  return new Promise((resolve, reject) => {
    LZMA.decompress(data, (result, error) => {
      if (error) {
        reject(error);
      }
      resolve(new Uint8Array(result));
    });
  });
}
