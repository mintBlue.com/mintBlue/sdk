import * as utils from '../../utils.js';
import {
  DerivationNonce,
  IAsymmetricKey,
  IDerivedKey,
  IKeyDerivationStrategy,
  IPublicKey,
  KeyInformation,
} from './interfaces.js';
import { SupportedKeyDerivationTypes } from '../module.js';
import { Bn, Ecdsa, KeyPair, Point, PrivKey, PubKey, Sig } from '@ts-bitcoin/core';
import { jose, JWKOrHexKeyToCryptoKeyAndJWK, crypto } from '../../utils.js';

const keyBitsLen = 32; // FIPS 186 B.4.1.
const defaultNonceIterations = 10000;

export class Secp256k1KeyDerivationStrategy implements IKeyDerivationStrategy {
  async fromRaw(rawKey: Uint8Array): Promise<Secp256k1Key> {
    const bitcoinKey = PrivKey.fromBuffer(Buffer.from(rawKey));

    return new Secp256k1Key(bitcoinKey, rawKey);
  }

  async create(masterKey: CryptoKey, salt: string): Promise<Secp256k1Key> {
    const rawMasterKey = await crypto.subtle.exportKey('raw', masterKey);

    const pathBytes = new TextEncoder().encode(salt);

    // Because a PBKDF2 output doesnt necessarily lie on the correct curve, we need to keep increasing iterations until we find a key that lies on the curve
    const derivedBytes = await utils.pbkdf2(
      new Uint8Array(rawMasterKey),
      pathBytes,
      keyBitsLen * 8,
      defaultNonceIterations,
    );

    console.log('Derived bytes before curve adjustment', {
      derivedBytes,
      derivedBytesLen: derivedBytes.length,
    });

    const derivedKey = bytesToPrivateKey(derivedBytes);
    const nonce = new DerivationNonce(salt, 10000);

    return new Secp256k1Key(derivedKey, derivedBytes, nonce);
  }

  async hydrate(masterKey: CryptoKey, nonce: DerivationNonce): Promise<IDerivedKey> {
    const rawMasterKey = await crypto.subtle.exportKey('raw', masterKey);

    const saltBytes = new TextEncoder().encode(nonce.salt);

    const derivedBytes = await utils.pbkdf2(new Uint8Array(rawMasterKey), saltBytes, keyBitsLen * 8, nonce.iterations);
    const derivedKey = bytesToPrivateKey(derivedBytes);

    return new Secp256k1Key(derivedKey, derivedBytes, nonce);
  }
}

export class Secp256k1PublicKey implements IPublicKey {
  constructor(private pubKey: PubKey) {}
  getPublicKeyHex(): Promise<string> {
    return Promise.resolve(this.pubKey.toHex());
  }
  getPublicKeyBytes(): Promise<Uint8Array> {
    return Promise.resolve(new Uint8Array(this.pubKey.toBuffer()));
  }

  async verify(signature: Uint8Array, message: Uint8Array): Promise<boolean> {
    const messageHash = await utils.sha256(message);

    return Ecdsa.verify(Buffer.from(messageHash), Sig.fromBuffer(Buffer.from(signature)), this.pubKey);
  }
}

export class Secp256k1Key implements IAsymmetricKey, IDerivedKey {
  constructor(
    private key: PrivKey,
    private secret?: Uint8Array,
    private derivation_path?: DerivationNonce,
  ) {}

  async getPublicKeyBytes(): Promise<Uint8Array> {
    const pubKey = await this.getPublicKey();
    return await pubKey.getPublicKeyBytes();
  }

  async getPrivateKeyBytes(): Promise<Uint8Array> {
    return new Uint8Array(this.key.toBuffer());
  }

  async toJWK({ includePrivate }: { includePrivate: boolean }) {
    const pubKey = PubKey.fromPrivKey(this.key);

    const jwk = {
      kty: 'EC',
      crv: 'secp256k1',
      alg: 'ES256K',
      use: 'enc', // TODO this should probably not be hardcoded here
      x: utils.base64.bufToUrlBase64(pubKey.point.getX().toBuffer()),
      y: utils.base64.bufToUrlBase64(pubKey.point.getY().toBuffer()),
      ...(includePrivate && { d: utils.base64.bufToUrlBase64(this.key.toBuffer()) }),
    };

    const thumbprint = await jose.calculateJwkThumbprint(jwk, 'sha256');
    return {
      ...jwk,
      kid: thumbprint,
    };
  }

  async toCryptoKey(): Promise<CryptoKey> {
    const hexPrivateKey = utils.hex.bufToHex(this.key.toBuffer());

    const { cryptoKey } = await JWKOrHexKeyToCryptoKeyAndJWK(hexPrivateKey, 'ECDSA', 'secp256k1');
    return cryptoKey;
  }

  async exportKeyInformation(): Promise<KeyInformation> {
    return {
      version: 1,
      curve: SupportedKeyDerivationTypes.SECP256K1,
      key: this.derivation_path
        ? {
            type: 'derived',
            from: 'master',
            nonce: this.derivation_path.toString(),
          }
        : {
            type: 'raw',
            rawKeyHex: utils.hex.bufToHex(this.secret!),
          },
      publicKey: await this.getPublicKey().then((p) => p.getPublicKeyHex()),
      fingerprint: utils.hex.bufToHex(await this.getFingerprint()),
    };
  }

  public toBitcoinKey(): PrivKey {
    return this.key;
  }

  async sign(message: Uint8Array): Promise<Uint8Array> {
    const messageHash = await utils.sha256(message);
    const signature = Ecdsa.sign(Buffer.from(messageHash), KeyPair.fromPrivKey(this.key));
    return signature.toBuffer();
  }

  async verify(signature: Uint8Array, message: Uint8Array): Promise<boolean> {
    const pubKey = await this.getPublicKey();

    return pubKey.verify(signature, message);
  }

  getPath(): DerivationNonce {
    return this.derivation_path!;
  }

  async getFingerprint(): Promise<Uint8Array> {
    return utils.sha256(await this.getPublicKeyBytes());
  }

  getPublicKey(): Promise<Secp256k1PublicKey> {
    return Promise.resolve(new Secp256k1PublicKey(PubKey.fromPrivKey(this.key)));
  }
}

/// Borrowed from ts-bitcoin
const bytesToPrivateKey = (bytes: Uint8Array) => {
  let bn: Bn;
  let condition: boolean;

  do {
    bn = new Bn().fromBuffer(Buffer.from(bytes));
    condition = bn.lt(Point.getN());
  } while (!condition);

  return PrivKey.fromBn(bn);
};
