### Derivation Service (src/lib/keys/DerivationService.ts)
The Derivation Service acts as an abstracted service layer currently implemented on top of the MintBlue Derivation API. It provides a flexible architecture that allows for swapping out the underlying storage layer according to the client's requirements. 

### Keys SDK Module (src/lib/keys/module.ts)
It abstracts away the old `mintblue.config.keys` object.
Its responsible for creating, importing, and searching for user keys, leveraging various cryptographic strategies. 
It integrates with the Derivation Service for storage and is responsible for syncing your keys with the service when you create or import a key

#### Key Strategies
Key strategies define the cryptographic algorithms and methods used for key derivation and management. The Keys SDK Module supports several strategies, catering to different security requirements and use cases.

##### AES256GCM
Implementation of the Symmetric AES256-GCM algorithm. 

NOTES:
- Need to figure out how to specify an IV for encryption as we currently dont allow extra methods on the encrypt function.
- Because all IKey implementations require to export a PublicKey, we just wrap the Fingerprint and call it a Public Key. 

##### P256
The P256 strategy encompasses two key derivation methods:

- ECDH (Elliptic Curve Diffie-Hellman): Facilitates secure key exchange over insecure channels, using the P-256 elliptic curve. Used for deriving shared keys to encrypt data over an insecure channel between 2 parties. Signing is not supported with this scheme

- ECDSA (Elliptic Curve Digital Signature Algorithm): Provides digital signing capabilities. Encryption and shared key derivation is not supported with this scheme.

##### SECP256K1 (Bitcoin)
The SECP256K1 strategy is tailored for Bitcoin and other blockchain applications. It utilizes the SECP256K1 elliptic curve, offering secure and efficient cryptographic operations for digital currency transactions.

#### Key Builder
The Key Builder, accessible through the keyBuilder() method of the KeysSDKModule, offers a stateful interface for key creation. It abstracts the complexity of cryptographic parameters, allowing users to specify key characteristics such as asymmetry and signability without delving into the underlying cryptographic details. This feature is particularly useful for users who require sensible defaults and ease of use in key generation. It was purpose built to help develop the MintBlue Key derivation UI in the Console.

## Extra Notes
The Keys SDK Module and its components are designed with extensibility in mind. The modular architecture ensures that the system can adapt to evolving cryptographic standards and client requirements. Further documentation and examples are available within the source code, providing in-depth guidance on leveraging the Keys SDK Module for secure key management.

For more detailed information and examples, refer to the source code in the ./lib/keys folder.