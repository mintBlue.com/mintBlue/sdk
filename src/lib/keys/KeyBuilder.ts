// A Builder pattern that allows you to choose to create a key
//
// Build an Asymmetric Encryption Key
// await new KeyBuilder(sdkKeyModule)
//  .setAsymmetric()
//  .setEncryptable()
//  .setName("Blah")
//  .build()
//
// Build a Symmetric Encryption Key
// await new KeyBuilder(sdkKeyModule)
//   .setSymmetric()
//   .setEncryptable()
//  .setName("Blah")
//   .build()
//
// Build an Asymmetric Signing Key
// await new KeyBuilder(sdkKeyModule)
//  .setAsymmetric()
//  .setSignable()
//  .setName("Blah")
//  .build()

import { AES256GCM_STRATEGY, KeysSDKModule, P256_ECDH_STRATEGY, P256_ECDSA_STRATEGY } from './module.js';
import { IKey, IKeyDerivationStrategy } from './strategies/index.js';

enum KeyTypes {
  Asymmetric = 'asymmetric',
  Symmetric = 'symmetric',
}

enum KeyUsages {
  Encryption = 'encryption',
  Signatures = 'signatures',
}

export class KeyBuilder {
  private name: string | undefined;
  private keyType: undefined | KeyTypes;
  private keyUsage: undefined | KeyUsages;

  constructor(private keyModule: KeysSDKModule) {}

  setAsymmetric() {
    this.keyType = KeyTypes.Asymmetric;
    return this;
  }

  setSymmetric() {
    this.keyType = KeyTypes.Symmetric;
    return this;
  }

  setSignable() {
    this.keyUsage = KeyUsages.Signatures;
    return this;
  }

  setEncryptable() {
    this.keyUsage = KeyUsages.Encryption;
    return this;
  }

  setName(name: string) {
    this.name = name;
    return this;
  }

  private matchKeyAndUsage(): IKeyDerivationStrategy {
    if (this.keyType === undefined) {
      throw new Error('You must choose between Asymmetric and Symmetric');
    }

    if (this.keyUsage === undefined) {
      throw new Error('You must choose between Signable and Encryptable');
    }

    if (this.keyType === KeyTypes.Asymmetric && this.keyUsage === KeyUsages.Encryption) {
      return P256_ECDH_STRATEGY;
    }
    if (this.keyType === KeyTypes.Asymmetric && this.keyUsage === KeyUsages.Signatures) {
      return P256_ECDSA_STRATEGY;
    }
    if (this.keyType === KeyTypes.Symmetric && this.keyUsage === KeyUsages.Encryption) {
      return AES256GCM_STRATEGY;
    }
    throw new Error(`Unsupported key type: ${this.keyType} and usage: ${this.keyUsage}`);
  }

  async build(): Promise<IKey> {
    const chosenCurve = this.matchKeyAndUsage();

    if (!this.name) {
      throw new Error('Name is required');
    }

    return this.keyModule.create(this.name, chosenCurve);
  }
}
