import { Reader, ActionDispatcher } from './types.js';
import { LogClass } from './utils.js';

/**
 * A reader that does nothing.
 *
 * This class represents a reader that performs no specific actions when initialized,
 * started, or stopped. It is often used as a placeholder or for testing purposes.
 *
 * @category Machine
 */
@LogClass()
export class NoopReader implements Reader {
  /**
   * Initializes the NoopReader.
   *
   * @param _actionDispatcher - A function to dispatch actions (not used in this implementation).
   */
  initialize(_actionDispatcher: ActionDispatcher): void {
    // This method does nothing in this implementation.
  }

  /**
   * Starts the NoopReader.
   *
   * @param _lastActionId - An optional last action identifier (not used in this implementation).
   */
  start(_lastActionId?: string): void {
    // This method does nothing in this implementation.
  }

  /**
   * Stops the NoopReader.
   */
  stop(): void {
    // This method does nothing in this implementation.
  }

  fetch(): void {}
}
