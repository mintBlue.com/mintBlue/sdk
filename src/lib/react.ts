import React, { createContext, useContext, useState, useEffect, ReactNode } from 'react';
import { MintblueOptions } from './types.js';
import { Mintblue } from './mintblue.js';

export interface MintblueContextType {
  mintblue: Mintblue | null;
  loading: boolean;
}

export const MintblueContext = createContext<MintblueContextType | null>(null);

export interface MintblueProviderProps {
  options: MintblueOptions; // Define a more specific type if you know the shape of the options object.
  children: ReactNode;
}

/**
 * `MintblueProvider` is a React component responsible for providing Mintblue SDK instance and its loading state to its child components.
 *
 * This provider uses the React Context API to allow child components to easily access and utilize the Mintblue SDK.
 *
 * @category React
 * @param {MintblueProviderProps} props - The properties accepted by the MintblueProvider.
 * @param {any} props.options - The options needed to initialize the Mintblue SDK.
 * @param {React.ReactNode} props.children - The child components that will consume the Mintblue SDK instance and loading state.
 *
 * @example
 * ```tsx
 * <MintblueProvider options={{ token: 'YOUR SDK TOKEN' }}>
 *   <YourChildComponent />
 * </MintblueProvider>
 * ```
 *
 * @returns {React.ReactNode} - Returns the child components wrapped with the Mintblue context.
 */
export const MintblueProvider: React.FC<MintblueProviderProps> = ({ options, children }: MintblueProviderProps) => {
  const [mintblue, setMintblue] = useState<Mintblue | null>(null);
  const [loading, setLoading] = useState<boolean>(true);

  useEffect(() => {
    async function createInstance() {
      try {
        const instance = await Mintblue.create(options);
        setMintblue(instance);
      } catch (error) {
        console.error('Failed to create Mintblue instance:', error);
        // Handle any other error actions if needed.
      } finally {
        setLoading(false);
      }
    }

    createInstance();
  }, [options]);

  return React.createElement(MintblueContext.Provider, { value: { mintblue, loading } }, children);
};

/**
 * A React hook that provides access to the `MintblueContext`.
 *
 * This hook allows components to easily fetch and utilize the Mintblue SDK instance and its associated loading state.
 * It should only be used within components wrapped by the `MintblueProvider`.
 *
 * @category React
 *
 * @throws Will throw an error if the hook is used outside of a `MintblueProvider`.
 *
 * @example
 * ```tsx
 * const { mintblue, loading } = useMintblue();
 * ```
 *
 * @returns {MintblueContextType} - Returns the current context value which contains the Mintblue SDK instance and its loading state.
 */
export const useMintblue = (): MintblueContextType => {
  const context = useContext(MintblueContext);

  if (!context) {
    throw new Error('useMintblue must be used within a MintblueProvider');
  }

  return context;
};

/** 
 * Example
 * 

import React from 'react';
import { MintblueProvider, useMintblue } from './path-to-your-provider-and-hook-file';

const App: React.FC = () => {
  return (
    <MintblueProvider options={{ token: 'your token' }}>
    <ChildComponent />
    </MintblueProvider>
  );
};

const ChildComponent: React.FC = () => {
  const { mintblue, loading } = useMintblue();

  if (loading) {
    return <div>Loading Mintblue...</div>;
  }

  // Now you can use mintblue methods and properties

  return (
    <div>
      {"your logic"}
    </div>
  );
};

export default App;

 */
