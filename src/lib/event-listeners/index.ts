import { Address, Tx } from '@ts-bitcoin/core';
import { ParsedPaymentFromEvent, ParsedPaymentToEvent } from '../types';

// WhatsonChain API URL
const API_BASE_URL = 'https://api.whatsonchain.com/v1/bsv/main';

// Function to fetch the previous transaction's details
async function fetchTxDetails(txid: string) {
  const response = await fetch(`${API_BASE_URL}/tx/hash/${txid}`);
  if (!response.ok) {
    throw new Error(`Failed to fetch transaction: ${txid}`);
  }
  return response.json();
}

/**
 * A collection of helper functions to parse EventListener transactions
 */
export const parseListenerTx = {
  /**
   * Parse a transaction and fetch the original UTXO for the trigger address, to get the satoshis sent
   */
  paymentFrom: async (rawtx: string, triggerAddress: string): Promise<Omit<ParsedPaymentFromEvent, 'txid'>> => {
    const bsvTx = Tx.fromHex(rawtx);
    const fromAddresses = bsvTx.txIns.map((txIn) => Address.fromTxInScript(txIn.script).toString());
    const toAddresses = bsvTx.txOuts.map((txOut) => ({
      address: Address.fromTxOutScript(txOut.script).toString(),
      amount: txOut.valueBn.toNumber(),
    }));

    const trigger = bsvTx.txIns.find((txIn) => triggerAddress === Address.fromTxInScript(txIn.script).toString());
    if (!trigger) {
      throw new Error('Trigger address not found in transaction input');
    }
    const txid = trigger.txHashBuf.toString('hex').match(/../g)!.reverse().join(''); // little-endian to big-endian
    const vout = trigger.txOutNum;
    const prevTx = await fetchTxDetails(txid);
    const output = prevTx.vout[vout];
    if (!output) {
      throw new Error(`Output index ${vout} not found in transaction ${txid}`);
    }

    const sentSatoshis = output.value * 1e8; // Convert BTC to satoshis

    return {
      senders: fromAddresses,
      receivers: toAddresses,
      sentSatoshis,
    };
  },
  paymentTo: async (rawtx: string, triggerAddress: string): Promise<Omit<ParsedPaymentToEvent, 'txid'>> => {
    const bsvTx = Tx.fromHex(rawtx);
    const fromAddresses = bsvTx.txIns.map((txIn) => Address.fromTxInScript(txIn.script).toString());
    const toAddresses = bsvTx.txOuts.map((txOut) => ({
      address: Address.fromTxOutScript(txOut.script).toString(),
      amount: txOut.valueBn.toNumber(),
    }));

    const trigger = toAddresses.find(({ address }) => address === triggerAddress);
    if (!trigger) {
      throw new Error('Trigger address not found in transaction output');
    }

    return {
      senders: fromAddresses,
      receivers: toAddresses,
      receivedSatoshis: trigger.amount,
    };
  },
};
