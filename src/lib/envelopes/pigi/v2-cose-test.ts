/*
import cbor from 'cbor';
import * as cose from 'cosette';

const wcrypto = cose.sign.webcrypto;
import { crypto, sha256hmac, sha512 } from '../../crypto';
import * as lzma from '../../lzma';

export async function create(
  data: Buffer,
  signingKey: CryptoKey,
  options?: {
    filename: string;
    mimetype: string;
  },
) {
  // Calculate the sha512 of the data
  const hash = await sha512(data);

  // Consider the first half as secret1
  const secret1 = hash.slice(0, 32);

  // COnsider the second half as secret2
  const secret2 = hash.slice(32);

  // Determine the data content encryption key by HMACing secret1 and secret2
  const cek = (await sha256hmac(secret1, secret2)).slice(0, 16);

  // We must export and import the key because else cosette will not recognize it as a cryptoKey
  const exported = await crypto.subtle.exportKey('jwk', signingKey);
  const imported = await wcrypto.subtle.importKey('jwk', exported, { name: 'ECDSA', namedCurve: 'P-256' }, true, [
    'sign',
  ]);

  // Pack the filename, mimetype and file contents together in CBOR array
  const filename = options.filename || 'file.dat';
  const mimetype = options.mimetype || 'application/binary';
  const cborEncodedFile = cbor.encodeOne([filename, mimetype, data], { highWaterMark: 1000000 } as any);

  // Compress with LZMA1 aloneEncoder
  const compressed = await lzma.compress(cborEncodedFile, 9);

  // Create inner layer 0 of encrypted file
  const l0 = await cose.encrypt.create(
    {
      p: { alg: 'A256GCM' },
      u: { kid: 'hmac(secret1,secret2)' },
    },
    compressed,
    {
      key: cek,
    },
    {},
  );

  // Create inner layer 1 of signature
  const layer1 = await cose.sign.create(
    {
      p: { alg: 'ES256' },
      u: { kid: 'signer' },
    },
    l0,
    {
      key: imported,
    },
  );

  // Create outer layer 2 of encryption
  const layer2 = await cose.encrypt.create(
    {
      p: { alg: 'A256GCM' },
      u: { kid: 'secret1' },
    },
    layer1,
    {
      key: secret1,
    },
    {},
  );

  return layer2;
}

/*
export async function parse(data: Buffer, secret1: Buffer, Secret2: Buffer, signingKey?: CryptoKey) {
  const layer2 = data;
  await cose.encrypt.read(layer2, {});
}
*/
